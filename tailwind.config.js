/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./html/**/*.{html,js}"],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      "halloween",
    ]
  },
  safelist: [
    {pattern: /bg-purple-500/},
  ]
}
package scraper

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/gocolly/colly"
	"github.com/rs/zerolog/log"

	"codeberg.org/pluja/kycnot-go/database"
	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

// Scrapes the ToS without using Javascript, so it is faster but fails with dynamic pages
func ScrapeTos(tosUrls []string) ([]string, error) {
	c := colly.NewCollector()
	c.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"

	terms := getKeyWords()

	var foundLinesMap map[string]bool

	c.OnRequest(func(r *colly.Request) {
		log.Print("Visiting", r.URL.String())
		foundLinesMap = make(map[string]bool) // Reset foundLinesMap on every new request
	})

	// Set error handler
	c.OnError(func(r *colly.Response, err error) {
		log.Print("Request URL:", r.Request.URL, "failed with statuscode:", r.StatusCode)
		if r.StatusCode == http.StatusNotFound {
			foundLinesMap["The ToS page may not be working, the bot got a 'Not Found' status."] = true
		} else if r.StatusCode == http.StatusForbidden {
			foundLinesMap["The bot could not access the ToS page. Maybe it is behing some kind of bot protection, like CloudFlare."] = true
		}
	})

	c.OnHTML("p, span, li, div", func(e *colly.HTMLElement) {
		for _, term := range terms {
			if strings.Contains(strings.ToLower(e.Text), strings.ToLower(term)) {
				if len(e.Text) > 8 {
					// Add text to foundLinesMap
					foundLinesMap[e.Text] = true
				}
			}
		}
	})

	for _, url := range tosUrls {
		if url != "" {
			err := c.Visit(url)
			if err != nil {
				log.Error().Err(err).Msg("Failed to visit URL")
				continue
			}
		}

	}

	// Convert foundLinesMap keys to slice
	foundLines := make([]string, 0, len(foundLinesMap))
	for line := range foundLinesMap {
		foundLines = append(foundLines, line)
	}
	return foundLines, nil
}

func ScrapScrap() {
	log.Info().Msg("Starting scraper...")
	// Scrape services on startup
	scrapeServices()
	// Get current time
	now := time.Now()
	// Find next midnight
	next := now.Add(time.Hour * 24)
	midnight := time.Date(next.Year(), next.Month(), next.Day(), 0, 0, 0, 0, next.Location())
	// Calculate duration until next midnight
	diff := midnight.Sub(now)
	// Create a new ticker
	ticker := time.NewTicker(diff)
	// Wait for the first tick
	<-ticker.C
	scrapeServices()
	// Set the ticker to tick every 24 hours after the first tick
	ticker.Reset(24 * time.Hour)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			// Call the function every 24h
			scrapeServices()
		}
	}
}

func scrapeServices() {
	services, err := database.GetAllListedServices()
	if err != nil {
		log.Error().Err(err).Msg("Failed to get all listed services")
		return
	}

	for _, service := range services {
		tosUrls, err := getTosUrlsSlice(service.TosUrls)
		if err != nil {
			log.Error().Err(err).Msgf("Failed to get TOS URLs slice for %v", service.Name)
			continue
		}

		if len(tosUrls) == 0 {
			log.Printf("No TOS URLs for %v", service.Name)
			continue
		}

		if service.TosScraperElements == "" {
			service.TosScraperElements = "default"
		}

		log.Printf("Scraping %v, %v", tosUrls, service.TosScraperElements)
		var foundLines []models.TosLine
		foundLines, err = ScrapeTosWithJs(tosUrls, service.TosScraperElements)
		if err != nil {
			log.Error().Err(err).Msg("Failed to scrape TOS")
			log.Warn().Msg("Trying again in 8 seconds...")
			// Wait 8 seconds and try again
			time.Sleep(8 * time.Second)
			foundLines, err = ScrapeTosWithJs(tosUrls, service.TosScraperElements)
			if err != nil {
				continue
			}
		}

		log.Printf("Found %v lines for service %s", len(foundLines), service.Name)
		if len(foundLines) == 0 && len(foundLines) != len(service.TosLines) {
			// Wait 8 seconds and try again
			log.Warn().Msg("Trying again in 8 seconds...")
			time.Sleep(8 * time.Second)
			foundLines, err = ScrapeTosWithJs(tosUrls, service.TosScraperElements)
			if err != nil || len(foundLines) == 0 {
				continue
			}
		}

		serviceLines, err := database.GetServiceTosLines(service.ID)
		if err != nil {
			log.Error().Err(err).Msg("Failed to get service TOS lines")
			serviceLines = make([]models.TosLine, 0)
		}

		serviceLines, err = getNewServiceLines(foundLines, serviceLines, service)
		if err != nil {
			log.Error().Err(err).Msg("Failed to get new service lines")
			continue
		}

		log.Printf("Updating service %s, found %v lines", service.Name, len(serviceLines))

		err = database.UpdateServiceTosLines(&service, serviceLines)
		if err != nil {
			log.Error().Err(err).Msg("Failed to update service TOS lines")
			continue
		}
	}
}

func getTosUrlsSlice(tosUrlsJson []byte) ([]string, error) {
	var tosUrls []string
	err := json.Unmarshal(tosUrlsJson, &tosUrls)
	if err != nil {
		log.Error().Err(err).Msg("Failed to unmarshal TOS URLs")
		return nil, err
	}
	return tosUrls, nil
}

func getNewServiceLines(foundLines, existingLines []models.TosLine, service models.Service) ([]models.TosLine, error) {
	foundLinesMap := make(map[string]models.TosLine)
	for _, line := range foundLines {
		foundLinesMap[line.Hash] = line
	}

	serviceLines := make([]models.TosLine, 0, len(existingLines))

	for _, existingLine := range existingLines {
		// If there is a line with the same hash, update it
		if line, ok := foundLinesMap[existingLine.Hash]; ok {
			log.Printf("%v: line '%s...' is still in the ToS", service.Name, existingLine.Hash)
			updateLine(&existingLine, &line)
			delete(foundLinesMap, existingLine.Hash)
		} else {
			// If there is a similar line, update it
			similarLine, found := findSimilarLine(existingLine, foundLines)
			if found {
				updateLine(&existingLine, similarLine)
				delete(foundLinesMap, similarLine.Hash)
				log.Printf("%v: similar line '%s...' is in the ToS", service.Name, existingLine.Hash)
			} else {
				// If there's no same or similar line, mark it as not in the ToS anymore
				existingLine.StillInTos = false
				log.Printf("%v line '%s...' is not in the ToS anymore", service.Name, existingLine.Hash)
			}
		}
		log.Printf("Updated line for service %s: %s", service.Name, existingLine.Hash)
		// Add the updated line to the service lines
		serviceLines = append(serviceLines, existingLine)
	}

	for _, newLine := range foundLinesMap {
		log.Printf("Found new line for service %s: %s", service.Name, newLine.Text)
		serviceLines = append(serviceLines, newLine)
	}

	return serviceLines, nil
}

func updateLine(existingLine, foundLine *models.TosLine) {
	existingLine.Text = foundLine.Text
	existingLine.Hash = foundLine.Hash
	existingLine.LastSeen = time.Now().Format("2006-01-02")
	existingLine.StillInTos = true
	log.Printf("Updating line for service: %s", foundLine.Text)
}

// Given a ToSLine, find if a similar line exists in a slice of ToSLines by comparing the Jaccard similarity
func findSimilarLine(existingLine models.TosLine, foundLines []models.TosLine) (*models.TosLine, bool) {
	for _, foundLine := range foundLines {
		if similarity := utils.JaccardSimilarity(existingLine.Text, foundLine.Text); similarity > 0.75 {
			log.Printf("Found similar existing line: %s", foundLine.Text)
			return &foundLine, true
		}
	}
	return nil, false
}

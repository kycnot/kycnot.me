package scraper

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/chromedp/chromedp"
	"github.com/rs/zerolog/log"

	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

const (
	DateFormat = "2006-01-02"
)

func ScrapeTosWithJs(urls []string, elements string) ([]models.TosLine, error) {
	log.Printf("Scraping %v for %v", urls, elements)
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	foundLinesMap := make(map[string]models.TosLine)
	var err error
	//var allTexts []string
	for _, url := range urls {
		if url == "" {
			continue
		}
		log.Printf("Scraping %s", url)

		var texts []string
		err = chromedp.Run(ctx, chromedp.Tasks{
			chromedp.Navigate(url),
			chromedp.WaitReady(`body`),
			chromedp.Sleep(8 * time.Second),
			textRetrivalTasks(elements, &texts),
		})
		if err != nil {
			log.Error().Err(err).Msgf("Error scraping %s", url)
			continue
		}

		for _, text := range texts {
			var textLines []string
			if len(text) > 1000 {
				textLines = strings.Split(cleanText(text), "\n")
			} else {
				textLines = []string{text}
			}
			for _, line := range textLines {
				if lineShouldBeAdded(line, foundLinesMap) {
					foundLine := addLine(line, url)
					foundLinesMap[foundLine.Hash] = foundLine
				}
			}
		}
	}

	if err != nil {
		return nil, err
	}

	return utils.MapToSlice(foundLinesMap), nil
}

func textRetrivalTasks(elements string, res *[]string) chromedp.Tasks {
	tasks := chromedp.Tasks{}

	if elements == "default" || elements == "" {
		elements = "li, span, p, pre, code, dt, dd, blockquote, tr, th, td, article, aside, details, summary"
	}

	// Create a Evaluate task for each tag
	jsSnippet := fmt.Sprintf(`Array.from(document.querySelectorAll('%s')).map(n => n.innerText || n.textContent)`, elements)
	tasks = append(tasks, chromedp.Evaluate(jsSnippet, res))

	return tasks
}

func cleanText(text string) string {
	cK := getConflictingKeywords()
	for _, kw := range cK {
		text = strings.ReplaceAll(strings.ToLower(text), kw, "")
	}

	text = strings.TrimSpace(text)
	text = strings.ReplaceAll(text, "\r", " ")
	text = strings.ReplaceAll(text, "\t", " ")
	text = strings.ReplaceAll(text, "  ", " ")
	return text
}

func lineShouldBeAdded(newText string, foundLinesMap map[string]models.TosLine) bool {
	if !isUnique(newText, foundLinesMap) {
		return false
	}

	keywords := getKeyWords()
	for _, keyword := range keywords {
		textToMatch := cleanText(newText)
		if matchKeyword(textToMatch, keyword) {
			return true
		}
	}

	return false
}

func matchKeyword(text, keyword string) bool {
	re := regexp.MustCompile(fmt.Sprintf(`\b%s\b`, regexp.QuoteMeta(strings.ToLower(keyword))))
	return re.MatchString(strings.ToLower(text))
}

func addLine(line, url string) models.TosLine {
	hash := md5.Sum([]byte(line))
	textHash := hex.EncodeToString(hash[:])
	fl := models.TosLine{
		Text:       line,
		Url:        url,
		Hash:       textHash,
		DateFound:  time.Now().Format(DateFormat),
		LastSeen:   time.Now().Format(DateFormat),
		StillInTos: true,
	}

	return fl
}

func getConflictingKeywords() []string {
	return []string{"no kyc", "anti-kyc", "nonsensical kyc procedures",
		"will never ask for kyc", "does not require kyc",
		"kyc can put people at risk", "no-kyc", "no registration or kyc", "kyc-free", "no kyc required"}
}

func getKeyWords() []string {
	return []string{"aml", "kyc", "know your customer", "anti-money laundering",
		"money laundering", "terrorist financing", "identify user",
		"user identification", "user identity", "provide required personal information",
		"provide personal information", "AML/KYC", "kyc/aml", "anti-money laundering",
		"identity card", "driver's license", "ID verification process", "financing of terrorism",
		"identity verification procedure", "suspend, interrupt or terminate", "Counter Financing Terrorism",
		"suspicious transactions", "transactions of suspicious nature", "monitor all transactions", "black lists",
		"suspicion detection", "source of funds", "identification documents", "verification of your identity"}
}

func isUnique(text string, foundLinesMap map[string]models.TosLine) bool {
	for _, line := range foundLinesMap {
		similarity := utils.JaccardSimilarity(text, line.Text)
		if similarity > 0.75 {
			return false
		}
	}
	return true
}

module codeberg.org/pluja/kycnot-go

go 1.20

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/allegro/bigcache/v3 v3.1.0
	github.com/chai2010/webp v1.1.1
	github.com/chromedp/chromedp v0.9.1
	github.com/dchest/captcha v1.0.0
	github.com/dustin/go-humanize v1.0.1
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-chi/httprate v0.7.4
	github.com/gocolly/colly v1.2.0
	github.com/joho/godotenv v1.5.1
	github.com/rs/cors v1.9.0
	github.com/rs/zerolog v1.29.1
	github.com/samber/lo v1.38.1
	github.com/xanzy/go-gitlab v0.88.0
	github.com/yuin/goldmark v1.5.4
	golang.org/x/text v0.9.0
	gorm.io/datatypes v1.2.0
	gorm.io/driver/sqlite v1.5.0
	gorm.io/gorm v1.25.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/antchfx/htmlquery v1.3.0 // indirect
	github.com/antchfx/xmlquery v1.3.17 // indirect
	github.com/antchfx/xpath v1.2.4 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/chromedp/cdproto v0.0.0-20230220211738-2b1ec77315c9 // indirect
	github.com/chromedp/sysutil v1.0.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.1.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.2 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/saintfish/chardet v0.0.0-20230101081208-5e3ef4b5456d // indirect
	github.com/stretchr/testify v1.8.3 // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/oauth2 v0.6.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)

require (
	github.com/go-sql-driver/mysql v1.7.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.15 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	golang.org/x/sys v0.8.0 // indirect
	gorm.io/driver/mysql v1.4.7 // indirect
)

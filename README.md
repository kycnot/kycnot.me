# kycnot.me website repository

This repository contains the source code for the [kycnot.me](https://kycnot.me) website.

## About

Read the about section on the website for more information: [kycnot.me/about](https://kycnot.me/about)

## Tech stack

- [Go](https://golang.org/)
    - [Go Templates](https://golang.org/pkg/html/template/)
    - [GORM](https://gorm.io/)
    - [Chi v5](https://go-chi.io/)

- [TailwindCSS](https://tailwindcss.com/)
    - [DaisyUI](https://daisyui.com/)
    - [Tabler Icons](https://tabler-icons.io/)

- [Sqlite3](https://www.sqlite.org/index.html)

## Development

### Requirements

- [Go](https://golang.org/)
- [Node](https://nodejs.org/en/)

### Setup

1. Clone the repository
2. Run `npm install` to install the required Node packages
    - Then `npx tailwindcss -i ./html/static/css/input.css -o ./html/static/css/style.css --watch` to start the TailwindCSS compiler
3. In another terminal run `go run . -dev` to start the server
4. Visit [localhost:3000](http://localhost:3000) in your browser

### Structure

- `main.go` - The main entry point of the application
- `database/` - Contains the database connection and the CRUD operations.
- `router/` - Contains the router and the handlers for the different routes.
- `html/` - Contains the HTML templates.
- `models/` - Contains the models for the database and application.
- `utils/` - Contains utility functions.
- `helpers/` - Contains helper functions for the router handlers.
- `scraper/` - Contains the code for the ToS scraper.

### Support

If you want to support the project, see how at [kycnot.me/about#support](https://kycnot.me/about#support)
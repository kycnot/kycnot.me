package database

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/rs/zerolog/log"

	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

type AuditList struct {
	List []models.AuditLog `json:"list"`
}

type AuditTableList struct {
	List []models.AuditTable `json:"list"`
}

// Gets the project tables from NocoDB and return as list
func GetTables() ([]models.AuditTable, error) {
	var auditTables AuditTableList

	// Try to get audit logs from cache
	body, err := utils.Cache.Get("audit-tables")
	if err == nil {
		log.Printf("Using cached audit-logs")
	} else {
		body, err = requestNocoEndpoint("tables")
		if err != nil {
			log.Error().Err(err).Msg("Error requesting NocoDB audit logs")
			return auditTables.List, err
		}
		_ = utils.Cache.Set("audit-logs", body)
	}

	// Create interface for unmarshalling
	err = json.Unmarshal(body, &auditTables)
	if err != nil {
		log.Error().Err(err).Msg("Error unmarshalling NocoDB audit logs")
		return auditTables.List, err
	}

	return auditTables.List, nil
}

// Gets the audit logs from NocoDB and return as list
func GetAuditLogs() ([]models.AuditLog, error) {
	var auditLogs AuditList

	// Try to get audit logs from cache
	var body []byte

	var al []models.AuditLog
	err := utils.GetStructFromCache("audit-logs", &al)
	if err == nil {
		log.Printf("Using cached audit-logs")
		return al, nil
	}

	// Get the tables from NocoDB
	body, err = requestNocoEndpoint("audits")
	if err != nil {
		log.Error().Err(err).Msg("Error requesting NocoDB audit logs")
		return auditLogs.List, err
	}

	// Unmarshall the audit logs
	err = json.Unmarshal(body, &auditLogs)
	if err != nil {
		log.Error().Err(err).Msg("Error unmarshalling NocoDB audit logs")
		return auditLogs.List, err
	}

	auditTables, err := GetTables()
	if err != nil {
		log.Error().Err(err).Msg("Error getting NocoDB audit tables")
		return auditLogs.List, err
	}

	var auditTableMap = make(map[string]string)
	for _, auditTable := range auditTables {
		auditTableMap[auditTable.ID] = auditTable.TableName
	}

	for i, auditLog := range auditLogs.List {
		// Convert auditLog Time string to golang Time
		t, err := time.Parse("2006-01-02 15:04:05-07:00", auditLog.CreatedAt)
		if err != nil {
			fmt.Println(err)
			return auditLogs.List, err
		}
		auditLogs.List[i].CreatedAt = humanize.Time(t)
		if auditLog.FKModelID != "" {
			title, err := GetRowName(auditTableMap[auditLog.FKModelID], auditLog.RowID)
			if err != nil {
				log.Error().Err(err).Msg("Error getting NocoDB audit row name")
				continue
				//return auditLogs.List, err
			}
			auditLogs.List[i].Description = strings.ReplaceAll(auditLog.Description, auditLog.RowID, title)
		}
	}

	go utils.SaveStructToCache(auditLogs.List, "audit-logs")
	return auditLogs.List, nil
}

func requestNocoEndpoint(endpoint string) ([]byte, error) {
	var body []byte
	tr := &http.Transport{}
	devModeEnabled, _ := strconv.ParseBool(os.Getenv("DEVELOPMENT"))
	if devModeEnabled {
		tr = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		log.Warn().Msg("Disabled: TLS checking for NOCODB request...")
	}
	client := &http.Client{Transport: tr}
	noco_host := os.Getenv("NOCO_HOST")
	noco_audits_project := os.Getenv("NOCO_AUDITS_PROJECT")
	noco_token := os.Getenv("NOCO_TOKEN")
	url := fmt.Sprintf("https://%s/api/v1/db/meta/projects/%s/%s", noco_host, noco_audits_project, endpoint)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error().Err(err).Msg("Error requesting NocoDB audit logs")
		return body, err
	}

	req.Header.Set("xc-token", noco_token)

	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("Error requesting NocoDB audit logs")
		return body, err
	}

	defer resp.Body.Close()

	body, err = io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("Error reading NocoDB audit logs")
		return body, err
	}

	return body, nil
}

package database

import (
	"fmt"
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	DBCon *gorm.DB
)

func InitDB() {
	var err error
	nerostrLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,   // Slow SQL threshold
			LogLevel:                  logger.Silent, // Log level
			IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,
		},
	)

	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local", os.Getenv("MARIADB_USER"), os.Getenv("MARIADB_PASSWORD"), os.Getenv("DB_HOST"), "3306", os.Getenv("MARIADB_DATABASE"))
	DBCon, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger:  nerostrLogger,
		NowFunc: func() time.Time { return time.Now().UTC() },
	})

	/*DBCon, err = gorm.Open(sqlite.Open("kycnot.db"), &gorm.Config{
		Logger:  nerostrLogger,
		NowFunc: func() time.Time { return time.Now().UTC() },
	})*/

	if err != nil {
		log.Printf("Error decoding body into struct %v", err)
		return
	}
}

package database

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"sort"
	"strings"

	"github.com/rs/zerolog/log"
	"gorm.io/datatypes"

	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

// // WRITE OPERATIONS // //

// Given a service, adds it to the database
func AddService(service *models.Service) error {
	log.Debug().Msgf("Adding service: %v", service)
	result := DBCon.Create(service)

	if result.Error != nil {
		return result.Error
	}
	return nil
}

// Given a point struct, adds it to the database
func AddPoint(point *models.Point) error {
	log.Debug().Msgf("Adding point: %v", point)
	result := DBCon.Create(point)

	if result.Error != nil {
		return result.Error
	}
	return nil
}

// Given a serviceId and a pointId, adds the point to the service
func AddPointToService(pointId, serviceId uint) error {
	log.Debug().Msgf("Adding point %v to service %v", pointId, serviceId)
	var service models.Service
	var point models.Point
	if err := DBCon.First(&service, serviceId).Error; err != nil {
		return fmt.Errorf("service not found: %w", err)
	}
	if err := DBCon.First(&point, pointId).Error; err != nil {
		return fmt.Errorf("point not found: %w", err)
	}
	if err := DBCon.Model(&service).Association("Points").Append(&point); err != nil {
		return fmt.Errorf("failed to add point to service: %w", err)
	}
	return nil
}

// Given a serviceId and a pointId, adds the point to the service
func RemovePointFromService(pointId, serviceId uint) error {
	log.Debug().Msgf("Removing point %v from service %v", pointId, serviceId)
	var service models.Service
	var point models.Point
	if err := DBCon.First(&service, serviceId).Error; err != nil {
		return fmt.Errorf("service not found: %w", err)
	}
	if err := DBCon.First(&point, pointId).Error; err != nil {
		return fmt.Errorf("point not found: %w", err)
	}
	if err := DBCon.Model(&service).Association("Points").Delete(&point); err != nil {
		return fmt.Errorf("failed to remove point from service: %w", err)
	}
	return nil
}

// Given a serviceID, adds a feedback to the database
func AddServiceNote(sID uint, fType int, text string) error {
	result := DBCon.Create(&models.Note{Type: fType, Text: text, ServiceID: sID})
	if result.Error != nil {
		log.Error().Err(result.Error).Msg("Failed to add service note")
		return result.Error
	}
	return nil
}

// Given a NoteID, removes it form the database
func RemoveNoteById(id uint) error {
	result := DBCon.Delete(&models.Note{}, id)
	if result.Error != nil {
		log.Error().Err(result.Error).Msg("Failed to remove note")
		return result.Error
	}
	return nil
}

// UPDATE OPERATIONS // //

func UpdateService(service *models.Service) error {
	log.Debug().Msgf("Updating service: %v", service)
	result := DBCon.Save(service)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func UpdatePoint(point *models.Point) error {
	log.Debug().Msgf("Updating point: %v", point)
	result := DBCon.Save(point)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func UpdateServiceTosLines(service *models.Service, lines []models.TosLine) error {
	log.Debug().Msgf("Updating service tos lines: %v", service.Name)
	jsonData, err := json.Marshal(lines)
	if err != nil {
		fmt.Println(err)
		return errors.New("failed to marshal tos lines")
	}
	service.TosLines = datatypes.JSON(jsonData)
	result := DBCon.Save(service)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

// // READ OPERATIONS // //

// Returns all services with type = typeFilter and status = utils.StatusApproved
func GetAllListedServicesByType(typeFilter string) ([]models.Service, error) {
	log.Debug().Msgf("Getting all services by type: %v", typeFilter)
	var services []models.Service

	// Preload the necessary relations and apply the status condition
	query := DBCon.Preload("Points").
		Preload("Flairs").
		Where("type = ? AND status = ?", typeFilter, utils.StatusApproved)

	// Execute the query
	result := query.Find(&services)

	if result.Error != nil {
		return services, result.Error
	}
	return services, nil
}

// Returns all services with status = utils.StatusApproved
func GetAllListedServices() ([]models.Service, error) {
	log.Debug().Msg("Getting all services")
	var services []models.Service

	// Preload the necessary relations and apply the status condition
	query := DBCon.Preload("Points").
		Preload("Flairs").
		Where("status = ?", utils.StatusApproved)

	// Execute the query
	result := query.Find(&services)

	if result.Error != nil {
		return services, result.Error
	}
	return services, nil
}

// Returns all services with status = utils.StatusApproved
func GetAllServices() ([]models.Service, error) {
	log.Debug().Msg("Getting all services")
	var services []models.Service

	// Preload the necessary relations and apply the status condition
	query := DBCon.Preload("Points").
		Preload("Notes").
		Preload("Flairs")

	// Execute the query
	result := query.Find(&services)

	if result.Error != nil {
		return services, result.Error
	}
	return services, nil
}

// Returns all services with status = utils.StatusPending
func GetAllPendingServices() ([]models.Service, error) {
	var services []models.Service

	// Preload the necessary relations and apply the status condition
	query := DBCon.Preload("Points").
		Where("status = ?", utils.StatusPending)

	// Execute the query
	result := query.Find(&services)

	if result.Error != nil {
		return services, result.Error
	}
	return services, nil
}

// Returns the service with the given id
func GetServiceById(id uint) (models.Service, error) {
	log.Debug().Msgf("Getting service by id: %v", id)
	var service models.Service
	result := DBCon.Preload("Points").Preload("Notes").Preload("Flairs").First(&service, id)

	if result.Error != nil {
		return service, result.Error
	}

	// Sort Points in memory
	sort.Slice(service.Points, func(i, j int) bool {
		return service.Points[i].Rating < service.Points[j].Rating
	})

	return service, nil
}

// Returns the service with the given name
func GetServiceByName(name string) (models.Service, error) {
	var service models.Service
	log.Debug().Msgf("Getting service by name: %v", name)
	result := DBCon.Preload("Points").Preload("Notes").Preload("Flairs").Where("name = ?", name).First(&service)

	if result.Error != nil {
		return service, result.Error
	}

	// Sort Points in memory
	sort.Slice(service.Points, func(i, j int) bool {
		return service.Points[i].Rating < service.Points[j].Rating
	})

	return service, nil
}

// Returns the service with the given domain name
func GetServiceByDomain(domain string) (models.Service, error) {
	var service models.Service
	log.Debug().Msgf("Getting service by domain: %v", domain)
	// Get any service that contains the domain name in its domain list
	result := DBCon.Preload("Points").Preload("Notes").Where("urls LIKE ?", "%"+domain+"%").First(&service)

	if result.Error != nil {
		return service, result.Error
	}
	return service, nil
}

// Returns the service ID with the given name
func GetServiceIdByName(name string) (uint, error) {
	var service models.Service
	log.Debug().Msgf("Getting service id by name: %v", name)
	result := DBCon.Where("name = ?", name).First(&service)
	if result.Error != nil {
		return 9999, result.Error
	}
	return uint(service.ID), nil
}

// Given a queryParams url.Values, returns all services with the given queries
func GetServicesByTypeAndQuery(serviceType string, queryParams url.Values) ([]models.Service, error) {
	var services []models.Service

	query := DBCon.Where("type = ?", serviceType)

	// Add full-text search to the query if 'q' parameter is present
	searchQuery := queryParams.Get("q")
	if searchQuery != "" {
		query = query.Where("name LIKE ? OR description LIKE ? OR tags LIKE ? OR category LIKE ?", "%"+searchQuery+"%", "%"+searchQuery+"%", "%"+searchQuery+"%", "%"+searchQuery+"%")
	}

	// Add currency filters to the query if any currency parameters are 'on'
	for _, c := range []string{"xmr", "btc", "ln", "cash", "fiat"} {
		if queryParams.Get(c) == "on" {
			query = query.Where(fmt.Sprintf("%v = ?", c), true)
		}
	}

	// Preload the Flairs and order Points by rating
	query = query.Preload("Points").Preload("Flairs").Where("status = ?", utils.StatusApproved)

	// Execute the query
	result := query.Find(&services)
	if result.Error != nil {
		return nil, result.Error
	}

	return services, nil
}

// Given a queryParams url.Values, returns all services with the given queries
func GetServicesByQuery(queryParams url.Values) ([]models.Service, error) {
	var services []models.Service

	query := DBCon.Model(&models.Service{})

	// Add full-text search to the query if 'q' parameter is present
	searchQuery := queryParams.Get("q")
	if searchQuery != "" {
		query = query.Where("name LIKE ? OR description LIKE ? OR tags LIKE ? OR category LIKE ?", "%"+searchQuery+"%", "%"+searchQuery+"%", "%"+searchQuery+"%", "%"+searchQuery+"%")
	}

	// Add currency filters to the query if any currency parameters are 'on'
	for _, c := range []string{"xmr", "btc", "ln", "cash", "fiat"} {
		if queryParams.Get(c) == "on" {
			query = query.Where(fmt.Sprintf("%v = ?", c), true)
		}
	}

	query = query.Preload("Points").Preload("Flairs").Where("services.status = ?", utils.StatusApproved)

	// Execute the query
	result := query.Find(&services)
	if result.Error != nil {
		return nil, result.Error
	}

	return services, nil
}

// Returns all points in the database ordered by rating
func GetAllPoints() ([]models.Point, error) {
	log.Debug().Msg("Getting all points")
	var points []models.Point
	result := DBCon.Order("rating").Find(&points)
	if result.Error != nil {
		return points, result.Error
	}
	return points, nil
}

func GetPointById(id uint) (models.Point, error) {
	log.Debug().Msgf("Getting point by id: %v", id)
	var point models.Point
	result := DBCon.First(&point, id)
	if result.Error != nil {
		return point, result.Error
	}
	return point, nil
}

// Returns all services with the given point id
func GetServicesWithPoint(pointId uint) ([]models.Service, error) {
	log.Debug().Msgf("Getting services with point: %v", pointId)
	var services []models.Service
	result := DBCon.Preload("Points").Preload("Flairs").Joins("JOIN service_points ON service_points.service_id = services.id").Where("service_points.point_id = ?", pointId).Find(&services)
	if result.Error != nil {
		return services, result.Error
	}
	return services, nil
}

// Returns the percentage of good, warning and bad points for a service
func GetServicePointsRatingPercentage(serviceID uint) (good, warning, bad float64, total int, err error) {
	var service models.Service
	result := DBCon.Preload("Points").First(&service, serviceID)
	if result.Error != nil {
		return 0, 0, 0, 0, errors.New("service not found")
	}

	var totalCount, goodCount, warningCount, badCount float64
	for _, point := range service.Points {
		if point.Rating == utils.PointRatingNeutral {
			continue
		}
		totalCount++
		switch point.Rating {
		case utils.PointRatingGood:
			goodCount++
		case utils.PointRatingWarning:
			warningCount++
		case utils.PointRatingBad:
			badCount++
		}
	}

	if totalCount == 0 {
		return 0, 0, 0, 0, nil
	}

	good = (goodCount / totalCount) * 100
	warning = (warningCount / totalCount) * 100
	bad = (badCount / totalCount) * 100
	return good, warning, bad, int(totalCount), nil
}

// Returns the TosLines datatypes.JSON as an array of models.TosLine
func GetServiceTosLines(serviceID uint) ([]models.TosLine, error) {
	var service models.Service
	result := DBCon.First(&service, serviceID)
	if result.Error != nil {
		return nil, result.Error
	}

	var lines []models.TosLine
	err := json.Unmarshal(service.TosLines, &lines)
	if err != nil {
		return nil, err
	}
	return lines, nil
}

// Returns the main field of the row_id on the given table
func GetRowName(table string, rowId string) (string, error) {
	table = utils.UpperCaseFirstLetter(table)
	if table == "" {
		return rowId, nil
	}
	var name string
	table = strings.ToLower(table)
	log.Printf("Getting row name for %v with id %v", table, rowId)
	result := DBCon.Raw("SELECT Title FROM "+table+" WHERE id = ?", rowId).Scan(&name)
	if result.Error != nil {
		result = DBCon.Raw("SELECT Name FROM "+table+" WHERE id = ?", rowId).Scan(&name)
		if result.Error != nil {
			log.Err(result.Error).Msg("Error getting row name")
			return "", result.Error
		}
	}
	if name == "" {
		log.Err(result.Error).Msg("No name or title field found")
		return "", errors.New("no name or title field found")
	}
	return name, nil
}

func GetActiveAnnouncements() ([]models.Announcement, error) {
	var announcements []models.Announcement
	result := DBCon.Where("active = ?", true).Find(&announcements)
	if result.Error != nil {
		return announcements, result.Error
	}
	return announcements, nil
}

package database

import (
	"encoding/gob"

	"github.com/rs/zerolog/log"

	"codeberg.org/pluja/kycnot-go/models"
)

func Migrate() {
	DBCon.AutoMigrate(models.Service{})
	DBCon.AutoMigrate(models.Point{})
	DBCon.AutoMigrate(models.Note{})
	DBCon.AutoMigrate(models.UserIssue{})
	DBCon.AutoMigrate(models.Announcement{})

	err := DBCon.Exec(`
		ALTER TABLE services
		ADD FULLTEXT INDEX services_fulltext (name, description, tags, category)
	`).Error
	if err != nil {
		log.Error().Err(err).Msg("Failed to add fulltext index to services table")
	}

	// Init gob cache, this is needed for caching structs in BigCache
	gob.Register(models.ExtendedService{})
}

func ConvertAllTimesToUTC() {
	services := []models.Service{}
	points := []models.Point{}

	DBCon.Find(&services)
	DBCon.Find(&points)

	for _, service := range services {
		service.CreatedAt = service.CreatedAt.UTC()
		service.UpdatedAt = service.UpdatedAt.UTC()
		DBCon.Save(&service)
	}

	for _, point := range points {
		point.CreatedAt = point.CreatedAt.UTC()
		point.UpdatedAt = point.UpdatedAt.UTC()
		DBCon.Save(&point)
	}
}

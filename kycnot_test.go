package main

import (
	"crypto/md5"
	"encoding/hex"
	"testing"

	"codeberg.org/pluja/kycnot-go/scraper"
)

func TestScrapeTosWithJs(t *testing.T) {
	tosLines, err := scraper.ScrapeTosWithJs([]string{"https://localmonero.co/faq"}, `div[itemprop="text"] > span`)
	if err != nil {
		t.Error(err)
	}

	if len(tosLines) == 0 {
		t.Error("No TOS lines found")
	}

	for _, line := range tosLines {
		if line.Text == "" {
			t.Error("Empty TOS line")
		}
		if line.Url == "" {
			t.Error("Empty TOS line URL")
		}
		if line.Hash == "" {
			t.Error("Empty TOS line hash")
		}
		if line.DateFound == "" {
			t.Error("Empty TOS line date found")
		}
	}

	if len(tosLines) < 1 {
		t.Errorf("Not enough TOS lines found. Found %v / Expected > %v", len(tosLines), 1)
	}

	expectedLine := "Even if you do your due diligence and only trade with reputable users there is no guarantee you'll not end up in a dispute situation. Here's something you can do to increase your chances:\n1. Request 2 photo ID scans of the user (i.e. passport and driver's license), make sure that the account name matches the ID.\n2. Tell the user to send you an email from the email account (maybe even tell them to put the Trade ID and something about the trade into the email).\n3. Charge very high premiums for trades. For example, 25% and higher. That way you get covered if 1 in 5 of your trades are scams (given equal trade amounts).\n4. Be wary of high trade amounts. Try to get a few lower amount trades with a trader first.\n"
	expectedHash := md5.Sum([]byte(expectedLine))
	expectedTextHash := hex.EncodeToString(expectedHash[:])
	if expectedTextHash != tosLines[0].Hash {
		t.Errorf("Wrong hash. Expected %v / Found %v", expectedTextHash, tosLines[0].Hash)
	}
	if expectedLine != tosLines[0].Text {
		t.Errorf("Wrong line. Expected %v / Found %v", expectedLine, tosLines[0].Text)
	}
}

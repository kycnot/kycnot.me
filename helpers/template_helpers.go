package helpers

import (
	"encoding/json"
	"errors"
	"math/rand"
	"time"

	"github.com/dustin/go-humanize"
	"github.com/rs/zerolog/log"
	"gorm.io/datatypes"
)

func HasCurrency(j datatypes.JSON, c string) (bool, error) {
	var result []string
	err := json.Unmarshal(j, &result)
	if err != nil {
		return false, err
	}
	for _, currency := range result {
		if currency == c {
			return true, nil
		}
	}
	return false, nil
}

// Given a datatypes.JSON, returns a slice of strings
func GetSlice(j datatypes.JSON) ([]string, error) {
	var result []string
	err := json.Unmarshal(j, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Given a datatypes.JSON, returns a random element from the slice
func GetRandomElement(j datatypes.JSON) (string, error) {
	var result []string
	if err := json.Unmarshal(j, &result); err != nil {
		log.Debug().Msgf("Error unmarshalling JSON: %v", err)
		return "", err
	}

	if len(result) == 0 {
		return "", errors.New("empty slice")
	}

	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	randomInt := r.Intn(len(result))
	randomElement := result[randomInt]
	return randomElement, nil
}

func IsEmptyDatatypeJSON(j datatypes.JSON) bool {
	var data []string
	if err := json.Unmarshal(j, &data); err != nil || len(data) == 0 || (len(data) == 1 && data[0] == "") {
		return true
	}
	return false
}

// Use Go-Humanize to format time
func HumanizeTime(t time.Time) string {
	return humanize.Time(t)
}

package helpers

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/samber/lo"
	"github.com/xanzy/go-gitlab"

	"codeberg.org/pluja/kycnot-go/database"
	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

func DownloadServiceLogo(serviceName string, force bool) {
	if _, err := os.Stat("html/static/img/" + serviceName + ".webp"); err == nil {
		log.Error().Err(err).Msgf("Failed to get service %v", serviceName)
		if !force {
			return
		}
	}

	result, err := database.GetServiceByName(serviceName)
	if err != nil {
		log.Error().Err(err).Msgf("Failed to get service %v", serviceName)
		return
	}

	log.Printf("Downloading logo for service %v: %v", serviceName, result.Name)

	if result.LogoUrl != "" {
		err = utils.DownloadImageAndConvertToWebp(result.LogoUrl, serviceName+".webp")
		if err != nil {
			log.Error().Err(err).Msgf("Failed to download logo for service %v", serviceName)
		}
		return
	}
}

func SearchServiceList(sList []models.Service, term string) (r []models.Service) {
	lTerm := strings.ToLower(term)
	// I use lo as it uses Golang genric types and is faster
	filteredList := lo.Filter(sList, func(s models.Service, index int) bool {
		if strings.Contains(strings.ToLower(s.Name), lTerm) ||
			strings.Contains(strings.ToLower(s.Description), lTerm) ||
			strings.Contains(strings.ToLower(s.Type), lTerm) ||
			strings.Contains(strings.ToLower(string(s.Urls)), lTerm) ||
			strings.Contains(strings.ToLower(string(s.Tags)), lTerm) ||
			strings.Contains(strings.ToLower(string(s.Onions)), lTerm) {
			return true
		}
		return false
	})
	return filteredList
}

func FilterServicesByCurrency(srvList []models.Service, currList []string) []models.Service {
	res := []models.Service{}
	for _, s := range srvList {
		for _, c := range currList {
			switch c {
			case "fiat":
				if s.Fiat {
					res = append(res, s)
					break
				}
			case "cash":
				if s.Cash {
					res = append(res, s)
					break
				}
			case "xmr":
				if s.Xmr {
					res = append(res, s)
					break
				}
			case "btc":
				if s.Btc {
					res = append(res, s)
					break
				}
			case "ln":
				if s.Ln {
					res = append(res, s)
					break
				}
			}
		}
	}
	return res
}

func GroupServicesByGrade(sList *[]models.ExtendedService) map[int][]models.ExtendedService {
	groups := lo.GroupBy(*sList, func(s models.ExtendedService) int {
		return s.Service.Grade
	})

	// Shuffle the groups
	for i, v := range groups {
		groups[i] = lo.Shuffle(v)
	}

	return groups
}

func SortServicesByGrade(sList *[]models.ExtendedService) {
	randSList := lo.Shuffle(*sList)

	sort.SliceStable(randSList, func(i, j int) bool {
		if (*sList)[i].Grade == -1 {
			return false
		}
		if (*sList)[j].Grade == -1 {
			return true
		}
		return (*sList)[i].Grade > (*sList)[j].Grade
	})
}

func CreateIssue(service models.Service, labels gitlab.Labels, body, title string) (string, error) {
	issue, err := CreateGitlabIssue(labels, body, title)
	if err != nil {
		log.Error().Err(err).Msg("Error creating issue:")
		return "", err
	}
	service.GitIssue = issue
	err = database.UpdateService(&service)
	if err != nil {
		log.Error().Err(err).Msg("Error updating service:")
		return "", err
	}

	return issue, nil
}

func UpdateIssue(service models.Service) error {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		log.Error().Err(err).Msg("Error creating gitlab client:")
		return err
	}
	// Get the issue from the service issue URL at service.Issue last part of the url after the last /
	issueId := strings.Split(service.GitIssue, "/")[len(strings.Split(service.GitIssue, "/"))-1]
	// Convet to int
	issueIdInt, err := strconv.Atoi(issueId)
	if err != nil {
		log.Error().Err(err).Msg("Error converting issue id to int:")
		return err
	}
	issue, _, err := git.Issues.GetIssue(os.Getenv("GITLAB_PROJECT_ID"), issueIdInt)
	if err != nil {
		log.Error().Err(err).Msg("Error getting issue:")
		return err
	}

	typeLabel := "Service"
	if service.Type == utils.ServiceTypeExchange {
		typeLabel = "Exchange"
	}

	if lo.Contains(issue.Labels, typeLabel) {
		// If the issue already has the labels, don't update it
		return nil
	}

	log.Printf("Updating %v with labels: %v", service.Name, typeLabel)

	_, _, err = git.Issues.UpdateIssue(os.Getenv("GITLAB_PROJECT_ID"), issueIdInt, &gitlab.UpdateIssueOptions{
		Labels: &gitlab.Labels{typeLabel},
	})

	return err
}

func CreateGitlabIssue(labels gitlab.Labels, body, title string) (string, error) {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		return "", err
	}

	issue, _, err := git.Issues.CreateIssue(os.Getenv("GITLAB_PROJECT_ID"), &gitlab.CreateIssueOptions{
		Title:       gitlab.String(title),
		Description: gitlab.String(body),
		Labels:      &labels,
	})

	if err != nil {
		return "", err
	}

	return issue.WebURL, nil
}

func DeleteGitlabIssues() {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		log.Error().Err(err).Msg("Error creating gitlab client:")
	}

	issues, _, err := git.Issues.ListProjectIssues(os.Getenv("GITLAB_PROJECT_ID"), &gitlab.ListProjectIssuesOptions{})
	if err != nil {
		log.Error().Err(err).Msg("Error listing issues:")
	}

	for len(issues) > 0 {
		for _, issue := range issues {
			time.Sleep(5 * time.Second)
			log.Printf("Deleting issue: %v", issue.IID)
			_, err := git.Issues.DeleteIssue(os.Getenv("GITLAB_PROJECT_ID"), issue.IID)
			if err != nil {
				log.Error().Err(err).Msg("Error deleting issue:")
			}
		}
		time.Sleep(1 * time.Second)
		issues, _, err = git.Issues.ListProjectIssues(os.Getenv("GITLAB_PROJECT_ID"), &gitlab.ListProjectIssuesOptions{})
		if err != nil {
			log.Error().Err(err).Msg("Error listing issues:")
		}
	}
}

func ConvertServiceToExtendedService(service models.Service) (models.ExtendedService, error) {
	// Check if extended service is in cache
	var extService models.ExtendedService

	// If the service was updated less than 10 minutes ago (cache time), don't use cache
	err := utils.GetStructFromCache(fmt.Sprintf("ext-service-%v", service.ID), &extService)
	if err == nil {
		// If the service was updated after the cache was created, return the cached service
		if utils.CompareServices(service, extService.Service) {
			log.Printf("Using cached service: %v", service.Name)
			// Sort Points in memory
			sort.Slice(extService.Points, func(i, j int) bool {
				return extService.Points[i].Rating < extService.Points[j].Rating
			})
			return extService, nil
		}
	}

	log.Printf("NOT-cached service: %v", service.Name)
	good, warning, bad, total, err := database.GetServicePointsRatingPercentage(service.ID)
	if err != nil {
		log.Error().Err(err).Msg("Error getting service points rating percentage:")
		return models.ExtendedService{}, err
	}

	// Calculate grade A, B, C, D, E
	if total < 3 {
		service.Grade = -1
	} else {
		utils.CalculateGrade(&service)
	}

	// Convert service.TosLines to []models.TosLine
	serviceTosLines := []models.TosLine{}
	// Check if service.TosLines is empty
	err = json.Unmarshal(service.TosLines, &serviceTosLines)
	if err != nil {
		log.Error().Err(err).Msg("Error unmarshalling service.TosLines:")
		serviceTosLines = []models.TosLine{}
	}

	// Create a map to pass to the template
	extService = models.ExtendedService{
		Service:  service,
		TosLines: serviceTosLines,
		Good:     good,
		Warning:  warning,
		Bad:      bad,
	}

	// Save the extended service to cache
	go utils.SaveStructToCache(extService, fmt.Sprintf("ext-service-%v", service.ID))
	return extService, nil
}

func ConvertServiceListToExtendedServices(sList *[]models.Service) []models.ExtendedService {
	extendedServices := make([]models.ExtendedService, len(*sList))
	for i, service := range *sList {
		eS, err := ConvertServiceToExtendedService(service)
		if err != nil {
			log.Error().Err(err).Msg("Error converting service to extended service:")
			continue
		}
		extendedServices[i] = eS
	}
	return extendedServices
}

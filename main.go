package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"

	"codeberg.org/pluja/kycnot-go/database"
	"codeberg.org/pluja/kycnot-go/helpers"
	"codeberg.org/pluja/kycnot-go/router"
	"codeberg.org/pluja/kycnot-go/scraper"
	"codeberg.org/pluja/kycnot-go/utils"
)

func main() {
	// Flags
	debug := flag.Bool("debug", false, "sets log level to debug")
	dev := flag.Bool("dev", false, "sets dev mode")
	nocache := flag.Bool("nocache", false, "disables cache")
	flag.Parse()

	// Logging
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		log.Printf("Debug mode enabled")
		log.Logger = log.Output(
			zerolog.ConsoleWriter{
				Out:        os.Stdout,
				TimeFormat: "02.01.2006 15:04:05",
			},
		).With().Caller().Logger()
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	if *dev {
		log.Logger = log.Output(
			zerolog.ConsoleWriter{
				Out:        os.Stdout,
				TimeFormat: "02.01.2006 15:04:05",
			},
		).With().Caller().Logger()
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		os.Setenv("DEVELOPMENT", "true")
		os.Setenv("SCRAPER", "false")
		log.Printf("DEV mode enabled")
	}
	if *nocache {
		log.Printf("Cache disabled")
		os.Setenv("CACHE", "false")
	}

	// Load .env file
	log.Info().Msg("Loading .env file.")
	err := godotenv.Load()
	if err != nil {
		log.Info().Msg("No .env file found, using environment variables")
	}

	// Database
	log.Info().Msg("Initialising Database.")
	database.InitDB()
	database.Migrate()

	// Create issues for all services
	if os.Getenv("CREATE_ISSUES") == "true" {
		go CreateIssues()
	}

	// Start background scraper if enabled
	scraperEnabled, _ := strconv.ParseBool(os.Getenv("SCRAPER"))
	if scraperEnabled {
		go scraper.ScrapScrap()
	}

	// Cache
	log.Info().Msg("Initialising Cache.")
	utils.InitCache()

	// Templates
	router.InitTemplates()

	// Init router
	r := router.InitRoutes()

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPatch},
	})

	handler := c.Handler(r)

	log.Info().Msg("Starting server on port 3000...")
	http.ListenAndServe(":3000", handler)
}

func CreateIssues() {
	services, err := database.GetAllServices()
	if err != nil {
		log.Error().Msgf("Error getting all services: %s", err)
		return
	}

	for _, service := range services {
		if service.GitIssue != "" {
			continue
		}
		// Avoid rate limiting
		time.Sleep(2 * time.Second)

		title := service.Name
		// Capitalize title words
		title = utils.Capitalize(title)
		log.Info().Msgf("Creating issue for service: %s", title)

		// Create a description for the issue with the service details
		description := fmt.Sprintf(`**Service name:** %s

**Service description:** %s

**Service URLs:** %s

**Service onions:** %s

**Service ToS URLs:** %s

**Service tags:** %s`, service.Name, service.Description, service.Urls, service.Onions, service.TosUrls, service.Tags)

		issue := ""
		if service.Type == utils.ServiceTypeExchange {
			issue, err = helpers.CreateGitlabIssue(gitlab.Labels{"Exchange"}, description, title)
		} else {
			issue, err = helpers.CreateGitlabIssue(gitlab.Labels{"Service"}, description, title)
		}
		if err != nil {
			log.Error().Msgf("Error creating issue: %s", err)
			continue
		}

		service.GitIssue = issue
		err = database.UpdateService(&service)
		if err != nil {
			log.Error().Msgf("Error updating service: %s", err)
			continue
		}
		log.Info().Msgf("~~~Done: %s", title)
	}
}

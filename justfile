default:
    @just --list

set dotenv-load

test:
    go test -v .

dev:
    go run . -dev

debug:
    go run . -debug

live-style:
    npx tailwindcss -i ./html/static/css/input.css -o ./html/static/css/style.css --watch

importdb sql_dump:
    sudo docker compose exec -iT database mysql -u$MARIADB_USER -p$MARIADB_PASSWORD $MARIADB_DATABASE < {{sql_dump}}

dumpdb sql_dump:
    sudo docker compose exec -iT database mariadb-dump --skip-lock-tables --routines --add-drop-table --disable-keys --extended-insert -u$MARIADB_USER -p$MARIADB_PASSWORD $MARIADB_DATABASE 2>&1 > {{sql_dump}}

update:
    #!/bin/bash
    git pull
    docker run -it --name node-builder -v ./:/app/ --rm node:latest /bin/bash -c "cd /app/; npm i; npx tailwindcss -o /app/html/static/css/style.css --minify"
    docker compose pull
    docker compose build
    docker compose up -d
    docker system prune -af
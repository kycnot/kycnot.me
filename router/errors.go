package router

import (
	"fmt"
	"net/http"
	"net/url"
)

const (
	// Error messages
	ErrInvalidCaptcha             = "Invalid captcha"
	ErrInvalidId                  = "Invalid ID"
	ErrServiceNotFound            = "Service not found"
	ErrPointNotFound              = "Point not found"
	ErrCouldNotGetLogs            = "Could not get audit logs, please try again later"
	ErrCouldNotGetServiceInfo     = "Could not get service info, please try again later"
	ErrCouldNotGetPointList       = "Could not get point list, please try again later"
	ErrCouldNotGetPendingServices = "Could not get pending services, please try again later"
	ErrCouldNotGetServices        = "Could not get services list, please try again later"
	ErrCouldNotCreateService      = "Could not create service, please try again later"
	ErrInvalidForm                = "Invalid form"
	ErrInvalidServiceType         = "Invalid service type"
	ErrInvalidPoint               = "Invalid point"
)

// Redirects to the service request page with an error message
func RedirectError(w http.ResponseWriter, r *http.Request, errMessage, redirTo string) {
	if errMessage == "500" {
		errMessage = "Internal server error"
	}
	if redirTo == "" {
		redirTo = "/"
	}
	http.Redirect(w, r, fmt.Sprintf("%s?error=%s", redirTo, url.QueryEscape(errMessage)), http.StatusSeeOther)
}

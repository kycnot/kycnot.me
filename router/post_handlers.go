package router

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/dchest/captcha"
	"github.com/rs/zerolog/log"
	"github.com/xanzy/go-gitlab"
	"gorm.io/datatypes"

	"codeberg.org/pluja/kycnot-go/database"
	"codeberg.org/pluja/kycnot-go/helpers"
	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

// Handles the request for a new service via the form
func PostRequestServiceForm(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		log.Error().Err(err).Msg("Error parsing form")
		RedirectError(w, r, ErrInvalidForm, "/request/service")
		return
	}

	valid, err := utils.McaptchaVerify(r.FormValue("mcaptcha__token"))
	if err != nil {
		log.Error().Err(err).Msg("Error verifying captcha")
		RedirectError(w, r, ErrInvalidCaptcha, "/request")
		return
	}

	if !valid {
		log.Warn().Msg("Invalid captcha!")
		RedirectError(w, r, ErrInvalidCaptcha, "/request")
		return
	}

	if !utils.IsValidServiceType(r.FormValue("serviceType")) {
		log.Warn().Str("serviceType", r.FormValue("serviceType")).Msg("Invalid serviceType")
		RedirectError(w, r, ErrInvalidServiceType, "/request")
		return
	}

	// kycLevel as integer
	kycLevel, err := strconv.Atoi(r.FormValue("kycLevel"))
	if err != nil || kycLevel > 3 || kycLevel < 0 {
		log.Warn().Str("kycLevel", r.FormValue("kycLevel")).Msg("Invalid kycLevel")
		RedirectError(w, r, ErrInvalidForm, "/request")
		return
	}

	service := models.Service{
		Name:        strings.ToLower(r.FormValue("serviceName")),
		Description: r.FormValue("description"),
		Type:        r.FormValue("serviceType"),
		KycLevel:    kycLevel,
		TosLines:    datatypes.JSON{},
		Status:      utils.StatusPending,
		Verified:    false,
	}

	currencies := r.PostForm["currencies[]"]
	urls := strings.Split(r.FormValue("siteUrls"), ",")
	onions := strings.Split(r.FormValue("torOnions"), ",")
	tosUrls := strings.Split(r.FormValue("tosUrls"), ",")
	tags := strings.Replace(r.FormValue("tags"), "  ", " ", -1)

	// Convert slices to JSON
	urlsJSON, _ := json.Marshal(urls)
	onionsJSON, _ := json.Marshal(onions)
	tosUrlsJSON, _ := json.Marshal(tosUrls)
	service.Urls = datatypes.JSON(urlsJSON)
	service.Onions = datatypes.JSON(onionsJSON)
	service.TosUrls = datatypes.JSON(tosUrlsJSON)
	service.Tags = tags
	service.Category = r.FormValue("category")
	service.TosLines = datatypes.JSON([]byte("[]"))

	for _, currency := range currencies {
		switch currency {
		case "fiat":
			service.Fiat = true
		case "cash":
			service.Cash = true
		case "xmr":
			service.Xmr = true
		case "btc":
			service.Btc = true
		case "ln":
			service.Ln = true
		}
	}

	log.Debug().Msgf("Adding new service: %v", service.Name)
	// Save service struct to the database
	err = database.AddService(&service)
	if err != nil {
		log.Error().Err(err).Msg("Error creating service")
		RedirectError(w, r, ErrCouldNotCreateService, "/request")
		return
	}

	// Get the points and add them to the service
	points := r.PostForm["points[]"]
	if len(points) > 0 {
		for _, point := range points {
			// Convert point string to uint64
			pointId, err := strconv.ParseUint(point, 10, 64)
			if err != nil {
				log.Error().Err(err).Msg("Error parsing point id")
				RedirectError(w, r, ErrInvalidPoint, "/request")
				return
			}

			database.AddPointToService(uint(pointId), service.ID)
		}
	}

	// Download the logo
	go utils.DownloadImageAndConvertToWebp(r.FormValue("logoUrl"), strings.ToLower(service.Name))

	title := utils.Capitalize(service.Name)
	// Create a description for the issue with the service details
	description := fmt.Sprintf(`**Service name:** %s

**Service description:** %s

**Service URLs:** %s

**Service onions:** %s

**Service ToS URLs:** %s

**Service currencies:** %s

**Service tags:** %s`, utils.Capitalize(service.Name), service.Description, strings.Join(urls, ", "), strings.Join(onions, ", "), strings.Join(tosUrls, ", "), strings.Join(currencies, ", "), tags)

	if service.Type == utils.ServiceTypeExchange {
		go helpers.CreateIssue(service, gitlab.Labels{"Exchange"}, description, title)
	} else {
		go helpers.CreateIssue(service, gitlab.Labels{"Service"}, description, title)
	}

	// Redirect to the request page
	http.Redirect(w, r, "/pending?message=Request%20successful!", http.StatusSeeOther)
}

// TODO: Add error handling with RedirectError function
// Handles the request for a new point via the form
func PostRequestPointForm(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, "Invalid data", http.StatusBadRequest)
		return
	}

	valid, err := utils.McaptchaVerify(r.FormValue("mcaptcha__token"))
	if err != nil {
		log.Error().Err(err).Msg("Error verifying captcha")
		RedirectError(w, r, ErrInvalidCaptcha, "/request")
		return
	}
	if !valid {
		log.Warn().Msg("Invalid captcha!")
		RedirectError(w, r, ErrInvalidCaptcha, "/request")
		return
	}

	if !captcha.VerifyString(r.FormValue("captchaId"), r.FormValue("captchaSolution")) {
		log.Warn().Msgf("Invalid captcha: %v for %v", r.FormValue("captchaSolution"), r.FormValue("captchaId"))
		RedirectError(w, r, ErrInvalidCaptcha, "/request")
		return
	}

	pointRating := utils.PointRatingNeutral
	if r.FormValue("pointRating") == "good" {
		pointRating = utils.PointRatingGood
	} else if r.FormValue("pointRating") == "bad" {
		pointRating = utils.PointRatingBad
	} else if r.FormValue("pointRating") == "warning" {
		pointRating = utils.PointRatingWarning
	}

	point := models.Point{
		Title:       strings.ToLower(r.FormValue("pointTitle")),
		Description: r.FormValue("description"),
		Rating:      pointRating,
		Status:      utils.StatusPending,
	}

	// Save point struct to the database
	database.AddPoint(&point)

	// Redirect to the request page
	http.Redirect(w, r, "/?message=Successfully added!", http.StatusSeeOther)
}

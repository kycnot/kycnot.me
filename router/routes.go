package router

import (
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dchest/captcha"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/httprate"
)

func InitRoutes() *chi.Mux {
	// Router
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	if os.Getenv("DEVELOPMENT") == "true" {
		// Parse templates on every request
		r.Use(func(next http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				InitTemplates()
				next.ServeHTTP(w, r)
			})
		})
	}

	fileServer := http.FileServer(http.Dir("html/static/"))
	r.Handle("/static/*", http.StripPrefix("/static/", cacheControl(fileServer)))

	r.Get("/", GetIndex)
	r.Get("/pending", GetPendingServices)
	r.Get("/points", GetPoints)
	r.Get("/request", GetRequestPage)
	r.Get("/search", GetIndex)
	r.Get("/service/{name}", GetService)
	r.Get("/point/{id}", GetPoint)
	r.Get("/about", GetAboutPage)
	r.Get("/audit", GetAuditPage)

	r.Group(func(r chi.Router) {
		// Limit requests to 2 per minute per IP
		r.Use(httprate.LimitByIP(5, 1*time.Minute))
		r.Get("/request/service", GetRequestServiceForm)
		r.Get("/request/point", GetRequestPointForm)
		r.Post("/request/service", PostRequestServiceForm)
		r.Post("/request/point", PostRequestPointForm)
	})

	r.Mount("/captcha/", captchaHandler())

	// Public API routes, rate limited to 100 requests per minute per IP
	r.Group(func(r chi.Router) {
		r.Use(httprate.LimitByIP(100, 1*time.Minute))
		r.Route("/api/v1", func(r chi.Router) {
			r.Get("/service/{name}", ApiGetService)
			r.Get("/domain/{name}", ApiGetServiceByDomain)
		})

		r.Get("/api/ping", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("pong"))
		})
	})

	return r
}

// Increase the cache of images
func cacheControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, ".webp") {
			w.Header().Set("Cache-Control", "max-age=31536000") // Set the Cache-Control header for .webp files
		}
		h.ServeHTTP(w, r) // Call the original handler
	})
}

func captchaHandler() http.Handler {
	router := chi.NewRouter()
	router.Handle("/{id}", captcha.Server(captcha.StdWidth, captcha.StdHeight))
	return router
}

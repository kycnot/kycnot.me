package router

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"

	"codeberg.org/pluja/kycnot-go/database"
	"codeberg.org/pluja/kycnot-go/helpers"
	"codeberg.org/pluja/kycnot-go/models"
)

func ApiGetService(w http.ResponseWriter, r *http.Request) {
	n := chi.URLParam(r, "name")
	var service models.Service
	// Check if service is in cache
	service, err := database.GetServiceByName(n)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	eS, err := helpers.ConvertServiceToExtendedService(service)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json")
	if j, e := json.Marshal(eS); e != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.Write(j)
	}
}

func ApiGetServiceByDomain(w http.ResponseWriter, r *http.Request) {
	d := chi.URLParam(r, "name")
	s, e := database.GetServiceByDomain(d)
	if e != nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	eS, e := helpers.ConvertServiceToExtendedService(s)
	if e != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if j, e := json.Marshal(eS); e != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.Write(j)
	}
}

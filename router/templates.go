package router

import (
	"encoding/json"
	"html/template"
	"strings"

	"github.com/rs/zerolog/log"
	"gorm.io/datatypes"

	"codeberg.org/pluja/kycnot-go/helpers"
)

var IndexTmpl *template.Template
var ServiceTmpl *template.Template
var PendingServicesTmpl *template.Template
var PointTmpl *template.Template
var PointsTmpl *template.Template
var AuditTmpl *template.Template
var RequestPageTmpl *template.Template
var ServiceRequestFromTmpl *template.Template
var PointRequestFormTmpl *template.Template

func createTemplate(files []string) *template.Template {
	funcMap := template.FuncMap{
		"toLower": strings.ToLower,
		"truncate": func(s string) string {
			if len(s) > 250 {
				return s[:250]
			} else {
				return s
			}
		},
		"slice": func(args ...interface{}) []interface{} {
			return args
		},
		"getSlice":    helpers.GetSlice,
		"randElem":    helpers.GetRandomElement,
		"isEmptyJson": helpers.IsEmptyDatatypeJSON,
		"html":        func(value string) template.HTML { return template.HTML(value) },
		"safeUrl": func(s string) template.URL {
			return template.URL(s)
		},
		"getObject": func(d datatypes.JSON) []string {
			var obj []string
			if err := json.Unmarshal(d, &obj); err != nil {
				log.Error().Err(err).Msg("Error unmarshalling JSON:")
			}
			return obj
		},
		"humanizeTime": helpers.HumanizeTime,
	}

	tmpl := template.New("").Funcs(funcMap)

	_, err := tmpl.ParseFiles(files...)
	if err != nil {
		log.Error().Err(err).Msg("Error parsing template:")
	}

	return tmpl
}

func InitTemplates() {
	IndexTmpl = createTemplate([]string{"html/base.html", "html/pages/index.html",
		"html/components/ServiceIcons.html", "html/components/ServiceCard.html"})

	ServiceTmpl = createTemplate([]string{"html/base.html", "html/pages/service.html",
		"html/components/ServiceIcons.html", "html/components/PointsTable.html",
		"html/components/ServiceNotes.html", "html/components/KycLevel.html"})

	PointTmpl = createTemplate([]string{"./html/base.html", "./html/pages/point.html", "./html/components/CompactServiceCard.html", "./html/components/ServiceIcons.html"})
	PointsTmpl = createTemplate([]string{"./html/base.html", "./html/pages/points.html", "./html/components/PointsTable.html"})

	PendingServicesTmpl = createTemplate([]string{"./html/base.html", "./html/pages/pending_services.html", "./html/components/ServiceCard.html", "./html/components/ServiceIcons.html"})

	AuditTmpl = createTemplate([]string{"html/base.html", "html/pages/audit.html"})

	RequestPageTmpl = createTemplate([]string{"./html/base.html", "./html/pages/request.html"})

	ServiceRequestFromTmpl = createTemplate([]string{"./html/base.html", "./html/pages/requests/form_service_request.html"})
	PointRequestFormTmpl = createTemplate([]string{"./html/base.html", "./html/pages/requests/form_point_request.html"})
}

package router

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strconv"

	"github.com/dchest/captcha"
	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog/log"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"

	"codeberg.org/pluja/kycnot-go/database"
	"codeberg.org/pluja/kycnot-go/helpers"
	"codeberg.org/pluja/kycnot-go/models"
	"codeberg.org/pluja/kycnot-go/utils"
)

func GetIndex(w http.ResponseWriter, r *http.Request) {
	queryParams := r.URL.Query()
	typeFilter := queryParams.Get("type")

	var serviceList []models.Service
	var err error

	if utils.IsValidServiceType(typeFilter) {
		serviceList, err = database.GetServicesByTypeAndQuery(typeFilter, queryParams)
		if err != nil {
			log.Error().Err(err).Msg("Error getting services by type and query:")
		}
	} else {
		serviceList, err = database.GetServicesByQuery(queryParams)
		if err != nil {
			log.Error().Err(err).Msg("Error getting services by query:")
		}
	}

	extendedServiceList := helpers.ConvertServiceListToExtendedServices(&serviceList)
	helpers.SortServicesByGrade(&extendedServiceList)
	//groupedServices := helpers.GroupServicesByGrade(&extendedServiceList)

	announcements, err := database.GetActiveAnnouncements()
	if err != nil {
		log.Error().Err(err).Msg("Error getting active announcements:")
		announcements = []models.Announcement{}
	}
	// Create a map to pass to the template
	varmap := map[string]interface{}{
		"Error":         queryParams.Get("error"),
		"Message":       queryParams.Get("message"),
		"Services":      extendedServiceList,
		"Type":          typeFilter,
		"Categories":    utils.ServiceCategories,
		"Announcements": announcements,
		"Filters": map[string]interface{}{
			"query": queryParams.Get("q"),
			"xmr":   queryParams.Get("xmr"),
			"btc":   queryParams.Get("btc"),
			"ln":    queryParams.Get("ln"),
			"cash":  queryParams.Get("cash"),
			"fiat":  queryParams.Get("fiat"),
		},
	}

	// Execute the template from templates.go
	if err = IndexTmpl.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func GetPendingServices(w http.ResponseWriter, r *http.Request) {
	services, err := database.GetAllPendingServices()
	if err != nil {
		log.Error().Err(err).Msg("Error getting pending services:")
		RedirectError(w, r, ErrCouldNotGetPendingServices, "/")
		return
	}

	extendedServiceList := helpers.ConvertServiceListToExtendedServices(&services)

	// Create a map to pass to the template
	varmap := map[string]interface{}{
		"Error":    r.URL.Query().Get("error"),
		"Services": extendedServiceList,
	}

	// Execute the template
	if err = PendingServicesTmpl.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		RedirectError(w, r, "500", "/")
	}
}

func GetPoints(w http.ResponseWriter, r *http.Request) {
	points, err := database.GetAllPoints()
	if err != nil {
		log.Error().Err(err).Msg("Error getting points:")
		RedirectError(w, r, ErrCouldNotGetPointList, "/")
		return
	}

	// Create a map to pass to the template
	varmap := map[string]interface{}{
		"Error":  r.URL.Query().Get("error"),
		"Points": points,
	}

	// Execute the template
	if err = PointsTmpl.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		RedirectError(w, r, "500", "/")
	}
}

func GetService(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")

	var service models.Service
	// Check if service is in cache
	service, err := database.GetServiceByName(name)
	if err != nil {
		log.Error().Err(err).Msg("Error getting service:")
		RedirectError(w, r, ErrServiceNotFound, "/")
		return
	}
	extService, err := helpers.ConvertServiceToExtendedService(service)
	if err != nil {
		log.Error().Err(err).Msg("Error getting service:")
		RedirectError(w, r, ErrCouldNotGetServiceInfo, "/")
		return
	}
	// Create a map to pass to the template
	varmap := map[string]interface{}{
		"Error":   r.URL.Query().Get("error"),
		"Message": r.URL.Query().Get("message"),
		"Service": extService,
	}
	utils.SaveStructToCache(varmap, fmt.Sprintf("service-svmp-%v", name))

	// Execute the template from templates.go
	if err = ServiceTmpl.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		RedirectError(w, r, "500", "/")
	}
}

func GetPoint(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	// Convert the id to a uint
	idUint, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		log.Error().Err(err).Msg("Error converting id to uint:")
		RedirectError(w, r, ErrInvalidId, "/")
		return
	}
	point, err := database.GetPointById(uint(idUint))
	if err != nil {
		log.Error().Err(err).Msg("Error getting point:")
		RedirectError(w, r, ErrPointNotFound, "/")
		return
	}

	services, err := database.GetServicesWithPoint(uint(idUint))
	if err != nil {
		log.Error().Err(err).Msg("Error getting services with point:")
		RedirectError(w, r, "500", "/")
		return
	}

	extendedServiceList := helpers.ConvertServiceListToExtendedServices(&services)
	helpers.SortServicesByGrade(&extendedServiceList)

	// Create a map to pass to the template
	varmap := map[string]interface{}{
		"Error":    r.URL.Query().Get("error"),
		"Message":  r.URL.Query().Get("message"),
		"Point":    point,
		"Services": extendedServiceList,
	}

	// Execute the template
	if err = PointTmpl.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		RedirectError(w, r, "500", "/")
	}
}

func GetAboutPage(w http.ResponseWriter, r *http.Request) {
	// Read the markdown file
	md, err := os.ReadFile("html/pages/about.md")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Convert markdown to HTML with goldmark and the auto ID extension
	mdParser := goldmark.New(
		goldmark.WithExtensions(extension.GFM),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
		goldmark.WithRendererOptions(
			html.WithHardWraps(),
			html.WithXHTML(),
		),
	)
	var htmlBuf bytes.Buffer
	err = mdParser.Convert(md, &htmlBuf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	html := htmlBuf.Bytes()

	// Convert markdown to HTML
	// Create a template with the custom functions
	tmpl := template.New("")

	files := []string{"./html/base.html", "./html/pages/about.html"}
	ts, err := tmpl.ParseFiles(files...)
	if err != nil {
		log.Error().Err(err).Msg("Error parsing template:")
		RedirectError(w, r, "500", "/")
		return
	}

	varmap := map[string]interface{}{
		"Error":   r.URL.Query().Get("error"),
		"Message": r.URL.Query().Get("message"),
		"Content": template.HTML(html),
	}

	// Execute the template
	if err = ts.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		RedirectError(w, r, "500", "/")
	}
}

func GetAuditPage(w http.ResponseWriter, r *http.Request) {
	// Create a template with the custom functions
	auditLogs, err := database.GetAuditLogs()
	if err != nil {
		log.Error().Err(err).Msg("Error getting logs:")
		RedirectError(w, r, ErrCouldNotGetLogs, "/")
		return
	}

	varmap := map[string]interface{}{
		"Error":   r.URL.Query().Get("error"),
		"Message": r.URL.Query().Get("message"),
		"Logs":    auditLogs,
	}

	// Execute the template
	if err = AuditTmpl.ExecuteTemplate(w, "base", varmap); err != nil {
		log.Error().Err(err).Msg("Error executing template:")
		RedirectError(w, r, "500", "/")
	}
}

func GetRequestPage(w http.ResponseWriter, r *http.Request) {
	varmap := make(map[string]interface{})
	varmap["Error"] = r.URL.Query().Get("error")
	varmap["Message"] = r.URL.Query().Get("message")

	err := RequestPageTmpl.ExecuteTemplate(w, "base", varmap)
	if err != nil {
		log.Print(err.Error())
		RedirectError(w, r, "500", "/")
	}
}

// Returns the html page for the service request form
func GetRequestServiceForm(w http.ResponseWriter, r *http.Request) {
	// Generate a captcha
	captchaID := captcha.New()

	points, err := database.GetAllPoints()
	if err != nil {
		log.Print(err.Error())
		RedirectError(w, r, "500", "/")
		return
	}

	varmap := make(map[string]interface{})
	varmap["Error"] = r.URL.Query().Get("error")
	varmap["Message"] = r.URL.Query().Get("message")
	varmap["CaptchaId"] = captchaID
	varmap["Points"] = points

	err = ServiceRequestFromTmpl.ExecuteTemplate(w, "base", varmap)
	if err != nil {
		log.Print(err.Error())
		RedirectError(w, r, "500", "/")
	}
}

// Returns the html page for the point request form
func GetRequestPointForm(w http.ResponseWriter, r *http.Request) {
	// Generate a captcha
	captchaID := captcha.New()
	varmap := make(map[string]interface{})
	varmap["Error"] = r.URL.Query().Get("error")
	varmap["Message"] = r.URL.Query().Get("message")
	varmap["CaptchaId"] = captchaID

	err := PointRequestFormTmpl.ExecuteTemplate(w, "base", varmap)
	if err != nil {
		log.Print(err.Error())
		RedirectError(w, r, "500", "/")
	}
}

![](/static/img/logos/logo_wide.svg)

## FAQ

- [FAQ](#faq)
- [Why?](#why)
- [What is KYC?](#what-is-kyc)
  - [Other acronyms of interest](#other-acronyms-of-interest)
- [Why only Bitcoin and Monero?](#why-only-bitcoin-and-monero)
- [Listings](#listings)
  - [Request](#request)
  - [What is a point?](#what-is-a-point)
  - [Filters](#filters)
  - [Scores](#scores)
    - [The scoring system](#the-scoring-system)
    - [Why is the score not an overall rating?](#why-is-the-score-not-an-overall-rating)
    - [How are the services ordered?](#how-are-the-services-ordered)
  - [ToS Checker](#tos-checker)
  - [KYC Levels](#kyc-levels)
  - [Verification](#verification)
  - [Discussions](#discussions)
  - [Badges](#badges)
- [About](#about)
  - [Support](#support)
    - [\> **Monero**:](#-monero)
    - [\> **Bitcoin**:](#-bitcoin)
  - [Contact](#contact)
  - [Social media](#social-media)
  - [About the site](#about-the-site)
  - [Tor and I2P](#tor-and-i2p)
- [Transparency](#transparency)
  - [Audit](#audit)
- [❗ Disclaimer](#-disclaimer)
- [Web trackers](#web-trackers)
  - [Javascript](#javascript)
- [API](#api)
  - [Endpoints](#endpoints)


## Why?

Cryptocurrencies were created to revolutionize the way we pay for goods and services, aiming to eliminate reliance on centralized entities such as banks and governments that control our economy.

Exchanges that enforce KYC (Know Your Customer) and AML (Anti-Money Laundering) rules operate similarly to traditional banks. Users are required to provide identification, such as a photo of their ID, to use these exchanges. Moreover, most of these exchanges are centralized, meaning that users do not own their keys. In short, this implies that the cryptocurrencies belong to the exchange and not the user. These requirements contradict the decentralized nature of cryptocurrencies.

With KYCNOT.ME, I hope to provide people with trustworthy alternatives for buying, exchanging, trading, and using cryptocurrencies without having to disclose their identity. This preserves the essence of cryptocurrencies, which is decentralized and self-governed.

---

## What is KYC?

**KYC** stands for **"Know Your Customer"**, a process designed to protect financial institutions against fraud, corruption, money laundering and terrorist financing. Or at least this is what they want you to believe.

The truth is that KYC is a threat to our freedom. KYC is a direct attack on our privacy and puts us in disadvantage against the governments. True criminals don't care about KYC policies. True criminals know perfectly how to avoid such policies. In fact, they normally use the FIAT system and don't even need to use cryptocurrencies. Banks are the biggest money launders, the [HSBC scandal](https://www.reuters.com/business/hsbc-fined-85-mln-anti-money-laundering-failings-2021-12-17/), [Nordea](https://www.reuters.com/article/us-nordea-bnk-moneylaundering-idUSKCN1QL11S) or [Swedbank](https://www.reuters.com/article/us-europe-moneylaundering-swedbank/swedbank-hit-with-record-386-million-fine-over-baltic-money-laundering-breaches-idUSKBN2163LU) are just some examples.

KYC only affects small individuals like you and me. It is an annoying procedure that obligates us to hand our personal information to a third party in order to buy, use or unlock our funds. We should start boycotting such companies. We should start using decentralized exchanges and decentralized wallets. We should start using cryptocurrencies as they were intended to be used.

### Other acronyms of interest

- **AML**: Anti-Money Laundering
- **CFT**: Combating the Financing of Terrorism
- **SoF**: Source of Funds

---


## Why only Bitcoin and Monero?

**Bitcoin**: it is the most well-known cryptocurrency. It's widespread and has the biggest market capitalization of all cryptocurrencies.

**Monero**: if digital cash was to exist, it should be like Monero. Fungible, private by design, fast and pretty low fees. Also, one of the oldest cryptocurrencies around.

No other currencies will be added. Most sites listed here also accept other cryptocurrencies, such as Ethereum or Litecoin.

---

## Listings

### Request

You can request a new listing by visiting the [Request](/request) page.

### What is a point?

A point is a feature that a service has. A point can be either good, a warning, bad or informational. Points are not limited, you can see a full list of the points available on the [Points](/points) page. You can also request new points from the [Request](/request) page, such points will be approved after a revision.

You can click on any point to see a detailed description of what it means. The point page also shows all the listings that have that point. This is also a useful way to find listings that have a specific feature.

### Filters

Filtering services in kycnot.me is very easy. In the main page, you will see 3 possible filters:

- **Type filter**: Lets you choose between seeing all the listings (default), only **exchanges**, or only **services**.
- **Search bar**: The search bar is the most powerful filter. You can use it to perform a full-text search on the listings. The search will look for the text you enter in the name, description, keywords and category. The search is not case-sensitive.
- **Currency filter**: Lets you choose between seeing all the listings (default), only listings that accept a certain currency. If you choose more than one option, it will show all the listings that accept **all** of the selected currencies.

### Scores

Services with a "**?**" as their score, have not been scored yet. This means that the listing still hasn't enough points for the score to be calculated.

#### The scoring system

KYCNOT.me calculates a NO-KYC score for each listing. This score is not an overall rating, it just represents how much NO-KYC friendly a listing is. The score is calculated based on the points that a listing has, currencies it accepts, Tor availability, and other factors that may affect the NO-KYC friendliness.

Scores are truncated to integer numbers. For this, you should see scores as **score groups**. For example, a score of 8.9 and a score of 8.1 are both in the same group, and they are both considered to be a score of 8. This is done to avoid giving a false sense of precision. Only scores that are above 9.5 are considered to be a score of 10.

**The score should be used as a reference only**. A listing with a low score may not mean that it is a bad listing. It may still be a plausible option for some people. You should always take your time to read the page of each listing and decide for yourself if it is a good option for you or not based on your needs.

#### Why is the score not an overall rating?

The score does not serve the purpose of rating a listing in terms of how good it is overall. What might qualify as a good listing for one person may not hold the same value for another. Since it's not fair to assign a subjective opinion to a listing, instead, the score is derived from objective criteria, thus, serving as an indicator of how good a listing is in terms of NO-KYC requirements, based on the data available.

**Example**: An exchange could have an awful UI/UX, but if it is very NO-KYC friendly, it will still get a high score. If the score was an overall rating, some people might give it a low score based on that user experience.

#### How are the services ordered?

The listings are sorted in a decreasing order by score. For listings that share the same score (tie), the order is random every time within that score range.

### ToS Checker

> ToS stands for "Terms of Service".

KYCnot.me has an automated tool that checks the ToS of all services and looks for keywords that may indicate that the website is performing KYC practices.

These checks run every 24h, you will see the detected suspicious ToS at the bottom of every listed service.

If a service has no ToS page, or the service has a ToS URL but is protected behind CloudFlare or any other anti-bot protection, the ToS checker will not be able to check it. Both cases will show a warning message at the bottom of the listing.

The detected ToS lines will be shown at the bottom of a service. 

This automated tool is not perfect, it may detect false positives, or non-relevant lines. If you see a false positive, you can report it on the Gitlab issue for that particular service. You can find the link to the Gitlab issue at every service's page in the "Discuss this service" button.

### KYC Levels

KYCnot.me has a KYC level system. These levels allow you to quickly identify the kind of KYC practices a service may have. There are 4 levels going from 0 to 3. The higher the level, the more invasive the KYC practices are. **You can click on the level on the service page to see a detailed description of what it means.**

### Verification

You will see that some services show a double blue tick. This means that the service has been tested personally by me. 

To check the service, I make a trade on the site, or test the service manually. With this, I can verify that the service does not perform KYC practices on regular basis, and that it is not a scam, at least at the time of the verification. 

These checks are done randomly and without prior notice to the service. For this reason, and as the list is in constant growth, not all services have been verified yet and it takes me some time to verify them.

A verified service does not mean that the service is safe to use blindly, it just means that at the time of the verification, the service was not performing KYC practices on regular basis, and that it was not a scam. It is still recommended to do your own research before using any service.

### Discussions

Each service has a dedicated Gitlab issue. You can find the link to the Gitlab issue at every service's page in the "Discuss this service" button.

The issues are used as a public discussion forum for each service. You can use it to discuss anything related to the service, such as if it should be listed or not, if it should have a particular point, etc. You can also leave a review of the service or report any issues you may find.

There is a dedicated project on Gitlab for the discussions: [gitlab.com/kycnot/services](https://gitlab.com/kycnot/services).

If you are a service owner, you can register on Gitlab and subscribe to the issue for your service to get notified on any new comments.

### Badges

Badges are small bubbles that provide a concise overview of crucial information about a listing. It's important to note that badges differ from [points](#what-is-a-point), as points represent features of a service, whereas badges are a way to give an adjective to a service.

Badges will be displayed in the main page, and in each service's page with a description of what it means.

Badges will only be applied to services after a **thorough discussion with the community**. If you believe a specific service deserves a particular badge, you can initiate a discussion on the Gitlab issue dedicated to that service. The "Discuss this service" button on each service's page will provide you with the link to the corresponding Gitlab issue.

Some of the badges are:

- **Controversial** - The service has been involved in some controversy.
- **Unstable** - The service is unstable, it may be down or not working properly.
- **Down** - The service is down.
- **Scam** - The service is considered to be a scam.

> More badges can be added. If you have an idea for a badge, you can open an issue on [Gitlab](https://gitlab.com/kycnot/kycnot.me).

---

## About

I'm an individual that goes by the pseudonym of Pluja. I maintain this project alone by myself, so there's no team or company behind this. You can find me on [Codeberg](https://codeberg.org/pluja) and [Gitlab](https://gitlab.com/pluja), here's a list of some of my projects that may interest you:

- [Awesome Privacy](https://github.com/pluja/awesome-privacy): A curated list of privacy-respecting services and software alternatives.
- [Web Whisper Plus](https://codeberg.org/pluja/web-whisper-plus): An audio transcription and subtitling suite that works directly from a web interface in your browser.
- [Blogo](https://gitlab.com/pluja/blogo) - A simple, fast and lightweight blogging engine written in Go.
- [Nerostr](https://codeberg.org/pluja/nerostr): Nostr paid relay, but with Monero.
- [Feetter](https://codeberg.org/pluja/feetter): Create, manage and sync anonymous Nitter feeds with Feetter.
- [I See Your Cash](https://codeberg.org/pluja/iseeyourcash) (UNMAINTAINED): See social network users cryptocurrency addresses and balances. Powered by transparent blockchains. It is no longer maintained due to lack of time, and issues with new Reddit and Twitter API debacles.

### Support

💚 If you want to support me or this project, you can do so by donating to the following addresses:

#### > **Monero**: 

> If your wallet supports OpenAlias, you can use *kycnot.me* as the address.

`82x6cn628oTUXV63DxBd6MJB8d997FhaSaGFvoWMgwihVmgiXYQPAwm2BCH31AovA9Qnnv1qQRrJk83TaJ8DaSZU2zkbWfM`

![](https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=monero:82x6cn628oTUXV63DxBd6MJB8d997FhaSaGFvoWMgwihVmgiXYQPAwm2BCH31AovA9Qnnv1qQRrJk83TaJ8DaSZU2zkbWfM)

#### > **Bitcoin**: 

[⚡ Tip via Lightning Network](https://getalby.com/p/kycnotme)

or on-chain:

> If your wallet supports OpenAlias, you can use *kycnot.me* as the address.

`bc1qfsgmpc7h6vk3y06c8pngu4p8um5zd8g2yvqfr6`

![](https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=bc1qfsgmpc7h6vk3y06c8pngu4p8um5zd8g2yvqfr6)

### Contact

For anything related directly with kycnot.me, please **open an issue** on [Gitlab](https://gitlab.com/kycnot/kycnot.me). It is the best and fastest way to contact me, and it also helps keeping track of everything.

For **any other inquiries**, you can contact me at:


- contact &#64; kycnot.me

> **❗ I will not reply to any emails related to kycnot.me listings or feature requests. Please open an issue as stated above**.

> If you use Tutanota, our conversation will be end-to-end encrypted.

You can also contact me on [SimpleX Chat](https://simplex.chat/) by using this [invitation link](https://simplex.chat/contact#/?v=1-2&smp=smp%3A%2F%2F0YuTwO05YJWS8rkjn9eLJDjQhFKvIYd8d4xG8X1blIU%3D%40smp8.simplex.im%2FcgKHYUYnpAIVoGb9lxb0qEMEpvYIvc1O%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAIW_JSq8wOsLKG4Xv4O54uT2D_l8MJBYKQIFj1FjZpnU%253D%26srv%3Dbeccx4yfxxbvyhqypaavemqurytl6hozr47wfc7uuecacjqdvwpw2xid.onion) for even more private communications.

### Social media

The only official social media accounts are:

- 🦣 [**Mastodon**](https://fosstodon.org/@kycnotme)
- 🪶 [**Nostr**](https://nostr.com/npub188x98j0r7l2fszeph6j7hj99h8xl07n989pskk5zd69d2fcksetq5mgcqf)
- 🐭 [**Lemmy**](https://lemmy.ml/u/kycnot)

The following accounts are **not** active, but they are still under my control:
- ❌ [Twitter](https://twitter.com/kycnot)
- ❌ [Reddit](https://old.reddit.com/u/hoiru)

### About the site

I am not a web developer, so I'm sure there are better ways to do this. I tried to always follow best practices and follow the effient way of doing things:

All the site logic and backend has been written in [Golang](https://go.dev/). It makes use of [Go Templates](https://golangforall.com/en/post/templates.html) for the frontend, this means it is fully rendered server side. This allows to have a fast and lightweight site, with no Javascript.

The frontend is built using [TailwindCSS](https://tailwindcss.com/) with [DaisyUI](https://daisyui.com/). The icons are from [Tabler Icons](https://tabler-icons.io/). The site is fully responsive and mobile friendly.

The database is an Sqlite3 database, with [GORM](https://gorm.io/) as the ORM. I make use of [BigCache](https://github.com/allegro/bigcache) for caching some queries and pages, so the server can handle more traffic.


[> The full source code is available on Gitlab](https://gitlab.com/kycnot/kycnot.me).

### Tor and I2P

KYCNOT.me is available on Tor at [kycnotme...qd.onion](http://kycnotmezdiftahfmc34pqbpicxlnx3jbf5p7jypge7gdvduu7i6qjqd.onion).

An I2P eepsite is also available at [kycnot...aq.b32.i2p](http://kycnot3724gujekedbe6pwoejfbnggdv26losox25iaokev5ctaq.b32.i2p).

---

## Transparency

In order to increase trust from visitors, KYCNOT.me strives to be as transparent as possible. Here are some of the ways in which I try to achieve this:

- All the code is open source and available on [Gitlab](https://gitlab.com/kycnot/kycnot.me).
- All the changes ever made to the code can be seen on the [Gitlab commits page](https://gitlab.com/kycnot/kycnot.me/-/commits/main).
- All the changes in the database, can be seen in the [audit logs](/audit) page.
- All the discussions for each service are public on the [Gitlab issue](https://gitlab.com/kycnot/services/-/issues) for each service.
- *SOON*: An endpoint for downloading the full database in JSON format will be available.

### Audit

The [audit page](/audit) displays any changes made to the database. This includes adding, removing or modifying services, points, badges, etc. It also shows the timestamp of the change. It updates automatically.

---

## ❗ Disclaimer

This is not financial advice. Always do your own research before using any service. This site is for informational purposes only. I'm not responsible for any loss of funds or any other damage that may occur by using any of the services listed here. Use at your own risk.

I always try to keep the information as accurate as possible, but I cannot guarantee that everything is correct. I can't also guarantee that any of the services listed here will not perform KYC in the future, or that it will turn into a scam. This is not under my control. For this reason, you should always do your own research before using any service.

If you find any incorrect information, please  open an issue on [Gitlab](https://gitlab.com/kycnot/kycnot.me).

---

## Web trackers

KYCNOT.me does not have trackers and never will. It does not make any third party connections from the frontend. No user data of any kind is collected. A "no-referrer" policy is enforced, so no site will know that you came from kycnot.me.

### Javascript

KYCNOT.me does not use Javascript. Everything is rendered server-side. This means that you can use KYCNOT.me with Javascript disabled.

The only exception is the [request](/request) page, which uses Javascript for validations of the form before submitting it. The Javascript is OpenSource and can be found [here](https://gitlab.com/kycnot/kycnot.me/-/tree/main/html/pages/requests).

---

## API

KYCNOT.me has an API that you can use to get the list of services, or the information for a particular service. 

- **API Limit**: 100 request per minute per IP address.

### Endpoints

`/api/v1/service/{name}`

This will return the information for the service with the name **{name}**. 

**Example**: [/api/v1/service/localmonero](/api/v1/service/localmonero)

`/api/v1/domain/{domain}`

This will return the information for the service with the domain **{domain}**.

**Example**: [/api/v1/domain/localmonero.co](/api/v1/domain/localmonero.co)
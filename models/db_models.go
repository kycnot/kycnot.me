package models

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Service struct {
	gorm.Model
	Name               string         `gorm:"not_null;unique" json:"name"`
	Description        string         `json:"description"`
	Grade              int            `json:"grade"`
	Status             int            `gorm:"not_null" json:"status"`
	KycLevel           int            `json:"kycLevel"`
	Verified           bool           `gorm:"not_null" json:"verified"`
	Type               string         `json:"type"`
	Category           string         `json:"category"`
	Tags               string         `json:"tags"`
	GitIssue           string         `json:"issue"`
	Referral           string         `json:"referral"`
	TosScraperElements string         `json:"tosScraperElements"`
	LogoUrl            string         `json:"logoUrl"`
	Xmr                bool           `json:"xmr"`
	Btc                bool           `json:"btc"`
	Ln                 bool           `json:"ln"`
	Fiat               bool           `json:"fiat"`
	Cash               bool           `json:"cash"`
	ListingNote        string         `json:"listingNote"`
	Points             []Point        `gorm:"many2many:service_points;" json:"points"`
	Flairs             []Flair        `gorm:"many2many:service_flairs;" json:"flairs"`
	Notes              []Note         `gorm:"foreignKey:ServiceID" json:"notes"`
	UserIssues         []UserIssue    `gorm:"foreignKey:ServiceID" json:"userIssues"`
	Urls               datatypes.JSON `gorm:"type:json" json:"urls"`
	TosLines           datatypes.JSON `gorm:"type:json" json:"tosLines"`
	Onions             datatypes.JSON `gorm:"type:json" json:"onions"`
	TosUrls            datatypes.JSON `gorm:"type:json" json:"tosUrls"`
}

type Point struct {
	gorm.Model
	Title       string `gorm:"not_null" json:"title"`
	Rating      int    `gorm:"not_null" json:"rating"`
	Status      int    `gorm:"not_null" json:"status"`
	Description string `json:"description"`
}

type Flair struct {
	gorm.Model
	Name        string `gorm:"not_null;unique" json:"name"`
	ColorCode   string `json:"colorCode"` // Hex color code for the flair
	Description string `json:"description"`
}

type Note struct {
	gorm.Model
	Type      int    `gorm:"not_null" json:"type"`
	Text      string `gorm:"not_null" json:"text"`
	Status    int    `gorm:"not_null" json:"status"`
	ServiceID uint   `json:"serviceId"`
}

type Announcement struct {
	gorm.Model
	Title       string `gorm:"not_null" json:"title"`
	Active      bool   `gorm:"not_null" json:"active"`
	Description string `json:"description"`
	Url         string `json:"url"`
}

type UserIssue struct {
	gorm.Model
	ServiceID   uint   `json:"serviceId"`
	Title       string `gorm:"not_null" json:"title"`
	Description string `gorm:"not_null" json:"description"`
	Status      int    `gorm:"not_null" json:"status"`
	Links       string `json:"url"`
	Images      string `json:"images"`
}

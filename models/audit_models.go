package models

type AuditLog struct {
	ID   string `json:"id"`
	User string `json:"user"`
	ProjectID string `json:"project_id"`
	FKModelID string `json:"fk_model_id"`
	RowID     string `json:"row_id"`
	Description string `json:"description"`
	Details     string `json:"details"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type AuditTable struct {
	ID          string `json:"id"`
	BaseID      string `json:"base_id"`
	ProjectID   string `json:"project_id"`
	TableName   string `json:"table_name"`
	Title       string `json:"title"`
	Type        string `json:"type"`
	Schema      string `json:"schema"`
	Deleted     string `json:"deleted"`
}

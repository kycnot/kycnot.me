package models

import "math/rand"

type TosLine struct {
	Text       string `json:"text"`
	Url        string `json:"url"`
	DateFound  string `json:"date_found"`
	LastSeen   string `json:"last_seen"`
	Hash       string `json:"hash"`
	StillInTos bool   `json:"still_on_tos"`
}

type ExtendedService struct {
	Service
	TosLines []TosLine `json:"tos_lines"`
	Good     float64   `json:"good"`
	Warning  float64   `json:"warning"`
	Bad      float64   `json:"bad"`
}

type ByGrade []ExtendedService

func (s ByGrade) Len() int {
	return len(s)
}

func (s ByGrade) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s ByGrade) Less(i, j int) bool {
	if s[i].Grade == -1 {
		return false
	}
	if s[j].Grade == -1 {
		return true
	}
	if s[i].Grade == s[j].Grade {
		return rand.Intn(2) == 0
	}
	return s[i].Grade < s[j].Grade
}

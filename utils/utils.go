package utils

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"hash/fnv"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"unicode"

	"github.com/chai2010/webp"
	"github.com/nfnt/resize"
	"github.com/rs/zerolog/log"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"

	"codeberg.org/pluja/kycnot-go/models"
)

func IsValidServiceType(t string) bool {
	switch t {
	case ServiceTypeService, ServiceTypeWallet, ServiceTypeExchange:
		return true
	}
	return false
}

func HashString(s string) string {
	h := sha256.New()
	h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}

func RandomString(length int) string {
	b := make([]byte, length)
	rand.Read(b)
	return hex.EncodeToString(b)
}

func DownloadImageAndConvertToWebp(url string, outputFilename string) error {
	log.Printf("Downloading image: %s\n", url)
	if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
		log.Printf("Error: ")
		return errors.New("invalid image URL")
	}

	if !strings.HasSuffix(outputFilename, ".webp") {
		outputFilename = fmt.Sprintf("%s.webp", outputFilename)
	}

	resp, err := http.Get(url)
	if err != nil {
		log.Printf("Error: ")
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Printf("Error: ")
		return errors.New("failed to download image")
	}

	img, _, err := image.Decode(resp.Body)
	if err != nil {
		log.Printf("Error: ")
		return err
	}

	img = resize.Resize(256, 256, img, resize.Lanczos3)

	downloadDir := "html/static/img/"
	outputPath := filepath.Join(downloadDir, outputFilename)
	out, err := os.Create(outputPath)
	if err != nil {
		log.Printf("Error: ")
		return err
	}
	defer out.Close()

	err = webp.Encode(out, img, &webp.Options{Quality: 80})
	if err != nil {
		log.Printf("Error: ")
		return err
	}

	return nil
}

func Capitalize(sentence string) string {
	return cases.Title(language.English, cases.Compact).String(strings.ToLower(sentence))
}

func UpperCaseFirstLetter(s string) string {
	for i, v := range s {
		return string(unicode.ToUpper(v)) + s[i+1:]
	}
	return ""
}

func PointContains(s string, point models.Point) bool {
	return strings.Contains(strings.ToLower(s), strings.ToLower(point.Title))
}

func ComputeHash(s models.Service) (uint64, error) {
	serviceBytes, err := json.Marshal(s)
	if err != nil {
		return 0, err
	}

	hasher := fnv.New64a()
	_, err = hasher.Write(serviceBytes)
	if err != nil {
		return 0, err
	}

	hash := hasher.Sum64()
	return hash, nil
}

// Returns true if the services are equal
func CompareServicesByHash(a models.Service, b models.Service) bool {
	a.Grade = b.Grade

	if a.Notes == nil {
		a.Notes = []models.Note{}
	}
	if b.Notes == nil {
		b.Notes = []models.Note{}
	}
	if a.Flairs == nil {
		a.Flairs = []models.Flair{}
	}
	if b.Flairs == nil {
		b.Flairs = []models.Flair{}
	}

	hashA, errA := ComputeHash(a)
	hashB, errB := ComputeHash(b)
	log.Printf("Hashes: \n\tA: %v\n\tB: %v", hashA, hashB)

	if errA != nil || errB != nil {
		log.Error().Msgf("Error computing hash: %v %v", errA, errB)
		return false
	}

	return hashA == hashB
}

func CompareServices(a models.Service, b models.Service) bool {
	if a.Name != b.Name || a.Status != b.Status ||
		a.Description != b.Description {
		return false
	}

	if a.LogoUrl != b.LogoUrl || a.Referral != b.Referral {
		return false
	}

	if a.Category != b.Category || a.Type != b.Type {
		return false
	}

	if a.Xmr != b.Xmr || a.Btc != b.Btc || a.Ln != b.Ln || a.Fiat != b.Fiat || a.Cash != b.Cash {
		return false
	}

	// compare datatypes.JSON fields
	if !bytes.Equal(a.TosLines, b.TosLines) || !bytes.Equal(a.TosUrls, b.TosUrls) ||
		!bytes.Equal(a.Urls, b.Urls) {
		return false
	}

	if len(a.Points) != len(b.Points) || len(a.Notes) != len(b.Notes) ||
		len(a.Flairs) != len(b.Flairs) {
		return false
	}

	// If none of the above checks failed, services are equal
	return true
}

func MapToSlice(foundLinesMap map[string]models.TosLine) []models.TosLine {
	foundLines := make([]models.TosLine, 0, len(foundLinesMap))
	for _, tosLine := range foundLinesMap {
		foundLines = append(foundLines, tosLine)
	}

	return foundLines
}

func JaccardSimilarity(a, b string) float64 {
	setA := make(map[string]bool)
	setB := make(map[string]bool)

	for _, word := range strings.Fields(a) {
		setA[word] = true
	}

	for _, word := range strings.Fields(b) {
		setB[word] = true
	}

	intersectionSize := 0
	for word := range setA {
		if setB[word] {
			intersectionSize++
		}
	}

	return float64(intersectionSize) / float64(len(setA)+len(setB)-intersectionSize)
}

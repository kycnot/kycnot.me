package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

type McaptchaResponse struct {
	Valid bool `json:"valid"`
}

func McaptchaVerify(token string) (bool, error) {
	payload := map[string]string{
		"token":  token,
		"key":    os.Getenv("MC_SITE_KEY"),
		"secret": os.Getenv("MC_SECRET_KEY"),
	}

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		fmt.Println("Error marshaling payload:", err)
		return false, err
	}

	resp, err := http.Post(fmt.Sprintf("https://%v/api/v1/pow/siteverify", os.Getenv("MC_ENDPOINT")), "application/json", bytes.NewBuffer(jsonPayload))
	if err != nil {
		fmt.Println("Error making HTTP POST request:", err)
		return false, err
	}
	defer resp.Body.Close()

	var result McaptchaResponse
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		fmt.Println("Error decoding response:", err)
		return false, err
	}

	return result.Valid, nil
}

package utils

import (
	"encoding/json"
	"fmt"
	"math"
	"os"
	"strconv"

	"github.com/rs/zerolog/log"

	"codeberg.org/pluja/kycnot-go/models"
)

func CalculateGrade(s *models.Service) int {
	gradeSteps := []string{}

	cacheKey := fmt.Sprintf("%v-score", s.ID)
	result, err := Cache.Get(cacheKey)
	if err == nil {
		log.Printf("Using cached grade for service %v", s.Name)
		cachedGrade, err := strconv.Atoi(string(result))
		if err == nil {
			s.Grade = cachedGrade
			return cachedGrade
		}
		log.Printf("Error converting cached grade to int for service %v", s.Name)
	}

	// good, warning, bad and isP2P are initialized to their zero values
	var good, warning, bad float64
	var isP2P bool

	needsAccount := false
	personalInfo := false
	counted := false
	for i := 0; i < len(s.Points); {
		p := s.Points[i]
		if p.ID == PointSemiCustodial || p.ID == PointClosedSource {
			s.Points = append(s.Points[:i], s.Points[i+1:]...)
			continue
		}

		if p.ID == PointAccountNeeded {
			needsAccount = true
		}

		if p.ID == PointNoPersonalInfo {
			personalInfo = true
		}

		switch p.Rating {
		case PointRatingBad:
			bad++
		case PointRatingWarning:
			warning++
		case PointRatingGood:
			good++
		}

		// If the service needs an account but doesn't require personal info
		if !counted && needsAccount && !personalInfo {
			log.Printf("Needs account but doesn't require personal info: %v", s.Name)
			warning--
			counted = true
		}

		if p.ID == PointP2P && s.Type != ServiceTypeService { // P2P
			isP2P = true
		}
		i++
	}

	total := good + warning + bad

	var grade float64
	grade = 7.75

	// Compute the penalty for bad and warning points
	penalty := ((warning * -0.2) + (bad * -0.4))
	grade += penalty
	gradeSteps = append(gradeSteps, fmt.Sprintf("Penalty: %v | Grade: %v", penalty, grade))

	var onions []string

	// If it supports anonymous payment methods
	if s.Xmr {
		grade += 0.25
		gradeSteps = append(gradeSteps, fmt.Sprintf("Supports XMR: +0.25 | Grade: %v", grade))
	}
	if s.Cash {
		grade += 0.25
		gradeSteps = append(gradeSteps, fmt.Sprintf("Supports CASH: +0.25 | Grade: %v", grade))
	}

	// If it does have a Tor url
	json.Unmarshal(s.Onions, &onions)
	if len(onions) > 0 && onions[0] != "" {
		grade += 0.15
		gradeSteps = append(gradeSteps, fmt.Sprintf("Has a Tor URL: +0.15 | Grade: %v", grade))
	}

	// If it has any suspicious tos lines
	// Can penalize up to 1.5 point
	var tosLines []map[string]interface{}
	if s.TosLines != nil {
		json.Unmarshal(s.TosLines, &tosLines)
		tosLinesCount := float64(len(tosLines))
		if tosLinesCount > 0 {
			grade -= 0.06 * math.Min(tosLinesCount, 15.0)
			gradeSteps = append(gradeSteps, fmt.Sprintf("Has suspicious ToS lines: -%v | Grade: %v", (0.1*tosLinesCount), grade))
		}
	}

	// Compute bonifications and penalizations based on the kyc level
	switch s.KycLevel {
	case 0:
		grade += 0.5
		gradeSteps = append(gradeSteps, fmt.Sprintf("KYC Level 0: +0.5 | Grade: %v", grade))
	case 2:
		grade -= 1.0
		gradeSteps = append(gradeSteps, fmt.Sprintf("KYC Level 2: -1.0 | Grade: %v", grade))
	case 3:
		grade -= 2.0
		gradeSteps = append(gradeSteps, fmt.Sprintf("KYC Level 3: -2.0 | Grade: %v", grade))
	}

	// If it is not P2P, it can't be 10
	if !isP2P && grade >= 10 && s.Type == ServiceTypeExchange {
		grade -= 1.0
		gradeSteps = append(gradeSteps, fmt.Sprintf("Not P2P, exchange can't be 10: -1.0 | Grade: %v", grade))
	}

	if isP2P && grade < 10 && s.Type == ServiceTypeExchange {
		grade += 1.0
		gradeSteps = append(gradeSteps, fmt.Sprintf("P2P: +1.0 | Grade: %v", grade))
	}

	if grade < 9.5 && s.Verified {
		grade += 0.25
		gradeSteps = append(gradeSteps, fmt.Sprintf("Verified: +0.25 | Grade: %v", grade))
	}

	// Convert grade to a scale of 0 to 10
	if grade > 9.5 {
		grade = 10
	}
	gradeInt := int(math.Trunc(grade))
	if gradeInt < 0 {
		gradeInt = 0
	} else if gradeInt > 10 {
		gradeInt = 10
	}

	// If the service has less than 4 points,
	// and the score is more than 9
	// it penalizes as there's not enough information
	if gradeInt >= 9 && total < 4 {
		gradeInt -= 1
		gradeSteps = append(gradeSteps, fmt.Sprintf("Less than 4 points: -1.0 | Grade: %v", grade))
	}

	if ok, _ := strconv.ParseBool(os.Getenv("DEVELOPMENT")); ok {
		log.Printf("\nGrade steps for %v", s.Name)
		for _, step := range gradeSteps {
			log.Printf("\t - %v", step)
		}
	}
	_ = Cache.Set(cacheKey, []byte(strconv.Itoa(gradeInt)))
	s.Grade = gradeInt
	return gradeInt
}

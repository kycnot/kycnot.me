package utils

import (
	"bytes"
	"context"
	"encoding/gob"
	"os"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/rs/zerolog/log"
)

var (
	Cache *bigcache.BigCache
)

func InitCache() {
	var err error
	// Cache for captcha handling
	Cache, err = bigcache.New(context.Background(), bigcache.DefaultConfig(10*time.Minute))

	if err != nil {
		log.Printf("Error initialising the cache: %v", err)
		return
	}
}

func SaveStructToCache(s interface{}, id string) error {
	// Encode struct to Bytes
	// Create a buffer
	buf := bytes.Buffer{}

	// Create a new encoder and send the struct to it
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(s)
	if err != nil {
		return err
	}

	_ = Cache.Set(id, buf.Bytes())
	return nil
}

func GetStructFromCache(id string, s interface{}) error {
	if os.Getenv("CACHE") == "false" {
		return bigcache.ErrEntryNotFound
	}

	b, err := Cache.Get(id)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buf)

	err = dec.Decode(s)
	if err != nil {
		return err
	}
	return nil
}

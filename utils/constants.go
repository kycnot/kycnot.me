package utils

// Status
const (
	StatusPending   int = 0
	StatusApproved  int = 1
	StatusOutdated  int = 2
	StatusInDispute int = 3
	StatusDelisted  int = -1
)

// Point ratings
const (
	PointRatingNeutral int = 3
	PointRatingGood    int = 2
	PointRatingWarning int = 1
	PointRatingBad     int = 0
)

// Note types
const (
	FeedbackTypeUser    int = 0
	FeedbackTypeNote    int = 1
	FeedbackTypeWarning int = 2
)

// Service types
const (
	ServiceTypeService  string = "service"
	ServiceTypeWallet   string = "wallet"
	ServiceTypeExchange string = "exchange"
)

type ServiceCategory string

// Categories
const (
	CategoryVPN     ServiceCategory = "vpn"
	CategoryMarket  ServiceCategory = "market"
	CategoryHosting ServiceCategory = "hosting"
	CategoryEmail   ServiceCategory = "email"
	CategoryGoods   ServiceCategory = "goods"
	CategoryTools   ServiceCategory = "tools"
	CategoryStorage ServiceCategory = "storage"
	CategoryPhone   ServiceCategory = "phone"
	CategoryOther   ServiceCategory = "other"
)

var ServiceCategories = map[string]ServiceCategory{
	"vpn":     CategoryVPN,
	"market":  CategoryMarket,
	"hosting": CategoryHosting,
	"email":   CategoryEmail,
	"goods":   CategoryGoods,
	"tools":   CategoryTools,
	"storage": CategoryStorage,
	"phone":   CategoryPhone,
	"other":   CategoryOther,
}

// announcement type
const (
	CHOOSING string = "choosing"
	FREE     string = "free"
	PAID     string = "paid"
	PINNED   string = "pinned"
	OFFICIAL string = "official"
)

const (
	// codeberg issue label IDs
	LabelExchange    = 121262
	LabelService     = 121263
	LabelOutdated    = 121264
	LabelScamWarning = 121265
	LabelPending     = 121304
	LabelListed      = 121305
)

const (
	PointCustodial      = 1
	PointAccountNeeded  = 3
	PointP2P            = 5
	PointClosedSource   = 7
	PointNoPersonalInfo = 8
	PointSemiCustodial  = 20
	PointStrictNoKyc    = 13
)
